# Praxis

![Praxis](./resources/images/logo.png)

Praxis is a QMK-based 36-key modal keyboard firmware with a few interesting
features to allow for a much easier programming and composition experience.

## Layout

Some notes about this layout, you should read this before continuing:

*   The modifier keys are presented on the upper-right of a key, which signifies
    that the key must be held to activate that modifier.
*   If there is a key at the lower-left of a modifier key, that means that if
    you tap it will activate that key (tapping any number of times and then
    holding will allow you to hold the "tapped" key).
*   If there is no key at the top-left, you should assume that when you hold
    "Rune Shift" you get the shifted variant of that key.
*   If there is a key at the top-left, then when you hold "Rune Shift", that is
    the character that will be printed.
*   The `Zetta` modifier (signified below by `Zet`) when held has two actions:
    it slows the mouse movement speed down, and if you press a function key
    you'll get the shift-variant of the function key (since the function keys
    all live on a shifted layer, we needed some way to differentiate).
*   Each operating mode has two layers, you can hold down "Layer Shift" to
    momentarily switch to the second layer. Common operations are on the main
    layer to minimize needing to hold "Layer Shift".
*   Keys marked as "★*" are called "Hyper Keys"; these are the key press `*`,
    while holding down `CTRL + ALT + META` (and if "Rune Shift" is pressed, then
    additionally `SHIFT`).

You can switch modes using the following key combos:

*   Press the two left-most thumb cluster buttons (`Del + LSft`) to switch
    directly to edit mode (regardless of your current mode).
*   Press the two right-most thumb cluster buttons (`Bksp + RSft`) to switch
    directly to review mode (regardless of your current mode).
*   Press the two inner-most thumb cluster buttons (`Spc + Enter`) to toggle
    between your current operating mode.

### Edit Mode

Edit mode is the default mode of the keyboard. It's intended to allow for text
entry and normal composition operations.

#### Layer 1

![Edit Mode Layer 1](./resources/maps/images/edit_1.png)

#### Layer 2 (Layer-Shifted)

![Edit Mode Layer 2](./resources/maps/images/edit_2.png)

### Review Mode

Review mode is the only other primary mode of the keyboard. It's intended to
allow for an easier time browsing and reviewing information.

#### Layer 1

![Review Mode Layer 1](./resources/maps/images/review_1.png)

#### Layer 2 (Layer-Shifted)

![Review Mode Layer 2](./resources/maps/images/review_2.png)

## Features

### A Modal Keyboard

This keyboard has two different primary modes; Edit and Review.

In edit mode, it works mostly like a normal (albeit tiny) keyboard, so you can
type and move the cursor around. Nothing too interesting.

In review mode, the layout becomes optimized for tasks you may want to do while
"reviewing" your content. The primary layer allows for mouse control, and all of
the buttons now are commands (`CTRL + <BUTTON>` or `CTRL + SHIFT + <BUTTON>`).

The layer shift of review mode still maintains a navigation cluster, and the
keys all become hyper commands (`CTRL + ALT + META + <BUTTON>` or
`CTRL + ALT + META + SHIFT + <BUTTON>`). This allows you to program your own
application-specific meaning into your shortcut keys without risking overlap
with the default keyboard shortcuts.

### Two (Different) Shift Keys

Yes, most normal keyboards have two shift keys, but in the Praxis design, the
shift keys are a primary key on the keyboard that is right under one of your
strongest digits (your thumbs).

The shift keys are not built identically, instead there is a "Layer Shift" (the
left shift) and a "Rune Shift" (the right shift). One of the design principles
is that the layer shift can make large fundamental changes to what your keyboard
keys do, whereas rune shift can only make changes that seem in some way related
to the keys for your current layer.

Both shift keys are momentary (meaning you must hold them down to use the layers
they provide).

### Smooth Mousekey Support

Since this keyboard intends on allowing you to (for the most part) ditch your
mouse, a lot of effort was put into making the mouse keys feel good.

The default functionality allows for smooth mouse movement with minimal lag, and
accounts for sub-pixel movement in the controller to make things feel smooth.
Mouse wheel support also has a "hiccup" feature to allow you to easily tap the
mouse wheel in a direction for constant movement.

Additionally, there are custom modifier keys (dubbed "Zetta" modifiers) which
you can hold down to slow the movement of the pointer or the wheel.

### Programmer-Centric WPM Calculator

The WPM calculator is programmed to consider different formats of words that are
common for programmers to type in.

For example, if you type `SQLServer`, the calculator will correctly identify
this as 2 words (`SQL` and `Server`). Similarly, other formats of words are also
supported:

*   `normal words`
*   `kebab-case`
*   `snake_case`
*   `TitleCase`

This allows for fairly accurate WPM calculation for programmers.

### Mod-Key Short Circuiting

Like most tiny keyboards, your mod keys are overlayed on other keys on the
keyboard, and you must hold the keys in to activate the mod. Unlike other
keyboards, there is some smarts to how these mod keys activate.

For example, if you are holding a modifier key, and you have tapped (pressed and
released) some other key, then we don't wait any longer on the modifier term to
decide if it's a hold or a tap - we instead immediately assume it was a hold and
activate the modifier.

This allows for mod-keys to feel very snappy and responsive.

### Combo-Key Short Circuiting

Like mod keys, combo keys (keys which are activated by pressing multiple keys at
roughly the same time) introduce lag.

Lag on keyboards is very noticeable, so short-circuiting was also added to combo
keys. If we are evaluating keys for a combo which is not a part of any known
combo, then the combo processing short-circuits and we abandon the combo and
immediately start processing the keys.

This allows for combo-keys to feel very snappy and responsive.

### OLED Display Support

The OLEDs for the keyboard show important information, and are optimized to only
update when they need to update.

The left keyboard will show the current operating mode of the keyboard, as well
as which modifier keys are pressed. The right keyboard will show your current
statistics (WPM (current), WPM (max), words, characters, keys), as well as a
graph of your WPM over the last 30 seconds.

During startup, you'll see these two screens with a firmware revision number:

![Left-Hand Splash Screen](./resources/oled/exports/splash_theory.png)
![Right-Hand Splash Screen](./resources/oled/exports/splash_praxis.png)

During normal operation, you'll see these two screens:

![Left-Hand Status Screen](./resources/oled/mocks/edit.png)
![Right-Hand Stats Screen](./resources/oled/mocks/stats.png)

Note: You can clear your current stats by pressing `Enter + RSft` at the same
time. This will also zero the graph so that you can get a clean start.

### Debug Event Log

If you press and hold the home and inner left thumb cluster buttons (`LSft` and
`Spc` respectively), you'll activate a special sub-mode that causes the OLED to
display the last 14 key events that occurred.

This is somewhat of a security risk, so the event log is cleared automatically
in the following situations:

*   `META + L` is being pressed. If the user is locking the computer, they are
    likely to be leaving their desk.
*   The keyboard has entered into an inactive state. You can identify this state
    by the OLED screens turning off (after about 30s).
*   If you press the keys `Enter + RSft` at the same time, it will clear your
    current stats and also clear the event log.

An example event log will look something like this (without the `//` comments):

```
xR1x6  // N tapped
xL1x2  // S tapped
xL1x3  // T tapped
^L3x3  // Bksp released
^L3x2  // RSft released
vL3x3  // Bksp pressed
vL3x2  // RSft pressed
xL3x3  // Bksp tapped
^L3x1  // LSft released
xL1x1  // R tapped
vL3x1  // LSft pressed
^R3x4  // RSft released
xL2x4  // V tapped
```

Obviously, it's hard (though not impossible) to figure out what was typed and
why. It's made extra hard by the fact the keyboard is used as both your mouse
and your keyboard. For example, I can see here there is a shift to review mode:

```
^L3x3  // Bksp released
^L3x2  // RSft released
vL3x3  // Bksp pressed
vL3x2  // RSft pressed
```

Then the keys `T` (mouse right), `N` (left-click), and `S` (mouse down) make a
little more sense. So we we're probably in edit mode before, in which case I can
see that I moved the cursor using `HOME` here:

```
^L3x1  // LSft released
xL1x1  // R tapped
vL3x1  // LSft pressed
```

And I had typed the capital key `V` just before that.

```
^R3x4  // RSft released
xL2x4  // V tapped
```

This feature exists so that when something odd happens with your keyboard, you
can stop and capture a log of your key presses to see what you did.

### Smart Modifiers

Modifier keys are processed using a modifier calculator that determines which
modifier keys should be pressed, and then resolving to press or release
modifiers to make the target modifier state the actual modifier state.

Shift keys are handled a special way by this calculator, prioritizing the shift
keys of the target character we want to print over whether or not you're holding
a shift key.

This is difficult to explain clearly, but what it means is that you can place
non-shifted character keys on shifted layers, because the modifier calculator
will correctly determine that "in order to press this key, I need to release
shift".

### Smart Window Switching

One problem that plagues other custom keyboards is that if you add a button for
window switching (`ALT + TAB` and/or `ALT + SHIFT + TAB`), then on some systems
this doesn't work since the Alt key is released so quickly.

Praxis avoids this issue by adding custom `NEXT_WINDOW` and `PREVIOUS_WINDOW`
actions. These actions will hold the required modifiers, tap `TAB`, and then it
will not release the modifiers until another unrelated key event is processed.

This has the effect of holding tab for you to keep the window switcher active. I
usually just release the `SHIFT` key to exit this mode where `ALT` is held,
since my window-switching actions are on a shifted layer. Since the release of
the shift key will cause the modifier engine to be updated.

Additionally, mouse keys won't ever update the modifier key engine, since the
mouse cannot directly influence which modifier keys are held (so you can still
move your mouse around to select something from the window switcher).

### Kill Window/Tab Operation

You may have noticed a "Kill" key on the review mode layers.

This key works a little special compared to other keys. If you hold it you enter
a sub-mode called "Kill Mode". Your left OLED screen will change to inform you
of this. When in this mode you are deciding whether or not you want to close a
tab or window.

The key doesn't just instantly close, instead it wants confirmation of
destructive action; you must press either of the inner thumb keys (`Spc` or
`Enter`) to confirm this action.

If you're only holding `Kill` and press one of these confirmation keys, it'll
send a common key shortcut for closing a tab (`CTRL + W`). However, if you're
holding shift when you press the confirmation key, it will instead send a common
key shortcut for closing a window (`ALT + F4`).

### Consistent Bracket Ordering

Common open/close brackets (`()`, `[]`, `{}`, and `^$`) are placed near one-
another so that you can press them easily. And they double as combo keys, so if
you press both of them at the same time you will always get them to be printed
in the correct order.

This is a small feature, but one that I quite enjoy.

## Building / Flashing

This is intended to be flashed on a Corne-style keyboard using QMK firmware.

Check out this repository under `qmk_firmware/keyboards/crkbd/rev1/keymaps/` in
its own directory named `praxis`. Then you can run the `flash.sh` script to
build and flash your keyboard.

I did have to make a few local changes to the `crkbd` code, this is because when
some features of QMK are disabled, it causes a build error. Whoever initially
wrote `crkbd` did not think anyone would define `NO_ACTION_TAPPING` (which is a
mostly reasonable thought, but Praxis does some weird stuff).

This mostly has to do with the default OLED code, but we override that anyways
so it's probably easiest to just delete whatever logic isn't compiling.

## Etc.

### Why are so many QMK features re-implemented in Praxis?

I originally set out to use as much of QMK as I could, but found very quickly
that I couldn't get what I wanted from the provided implementation. There was
always something special I needed or the feature didn't work 100% how I wanted.

How I ended up doing it is just use a bunch of user-keys (QMKs feature for
allowing you to programmatically define what a key should do), and then in the
firmware we use our own keymap to handle our use-case specially.

While this took a lot of work, I think the effort shows because the features all
work in a way that I would consider best in class. Often working better than the
QMK counterpart because of our weird design constraints.

### Why is ________ coded in a funny way?

TL;DR: If you see something that's coded weird, it's very possible that it had
to be coded that way for size-constraint purposes.

We don't have a ton of space to play with in this firmware, so we did a lot of
things to reduce the size of the final binary. In some cases, the strange format
of the code was due to correctness though; such as how the state machine for key
handling uses a bunch of bits in a `uint8_t` to determine which states have been
visited.

Anyways, usually it's a size thing.

### How did you convert the PNGs to character arrays of data?

I used this tool: https://javl.github.io/image2cpp/

You'll have to set the "Draw Mode" to "Vertical - 1 bit per pixel". also the
tool has some issues with processing images of different sizes properly, so you
should process only images of the same size simultaneously.

### How did you create images of the keyboard layout?

I used this tool: http://www.keyboard-layout-editor.com/

### Why is it called "Praxis"?

The name originates from the original keyboard design where I was testing some
ideas. At that time I was not writing custom firmware, and instead using a GUI
QMK configuration tool. I thought I'd forget my new layout a lot, so I named
the layout "Triton" after a forgetful character from Xenoblade Chronicles 3
(which I was playing at the time).

After a while, I got the idea for creating a layout like this one. I knew it'd
probably require custom firmware to perfect, but it seemed like a neat idea. I
decided to use the characters *Theory* and *Praxis* from Xenoblade Chronicles 2:

*   *Theory* was the name of the first prototype that I did for this layout
    using the GUI tool. I went as far as I could without custom firmware. You
    can get pretty far this way, but some things annoyingly won't work like you
    expect.
*   *Praxis* is the name of the layout using custom firmware to smooth out the
    edges and fix the bugs from the Theory version of the layout. So you can
    roughly think of the difference between them as "vanilla QMK" vs "custom".

I have no special affinity for Theory or Praxis, I just wanted to continue the
naming scheme, and figured using these names was appropriate for the task at
hand (while still being kind of a fun way to name things), and I like the
designs of the characters.

It's a custom keyboard that probably only I will use, might as well have fun!
