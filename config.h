/*******************************************************************************
 * PRAXIS QMK CONFIG
 ******************************************************************************/
#pragma once

// =============================================================================
// KEYBOARD CONFIGURATION
// =============================================================================

// -----------------------------------------------------------------------------
// The master keyboard is the left keyboard.
// -----------------------------------------------------------------------------
#ifndef   MASTER_LEFT
#define   MASTER_LEFT
#endif // MASTER_LEFT

// =============================================================================
// QMK FEATURES RE-IMPLEMENTED BY PRAXIS
// =============================================================================

// -----------------------------------------------------------------------------
// Praxis uses the pointer driver to implement its own mousekeys more smoothly.
// -----------------------------------------------------------------------------
#ifdef    MOUSEKEY_ENABLE
# undef   MOUSEKEY_ENABLE
#endif // MOUSEKEY_ENABLE

// -----------------------------------------------------------------------------
// Praxis has a very opinionated view of layers, so it implements layers itself.
// -----------------------------------------------------------------------------
#ifndef   NO_ACTION_LAYER
# define  NO_ACTION_LAYER
#endif // NO_ACTION_LAYER

// -----------------------------------------------------------------------------
// Praxis defines tapping keys on its own to work with the custom layer system.
// -----------------------------------------------------------------------------
#ifndef   NO_ACTION_TAPPING
# define  NO_ACTION_TAPPING
#endif // NO_ACTION_TAPPING

// =============================================================================
// QMK FEATURES USED BY PRAXIS
// =============================================================================

// -----------------------------------------------------------------------------
// Required for some reason, or else the audio control keys will not work.
// -----------------------------------------------------------------------------
#ifndef   EXTRAKEY_ENABLE
# define  EXTRAKEY_ENABLE
#endif // EXTRAKEY_ENABLE

// -----------------------------------------------------------------------------
// Required because of a build error when this is disabled.
// -----------------------------------------------------------------------------
#ifndef   RGBLIGHT_ENABLE
# define  RGBLIGHT_ENABLE
#endif // RGBLIGHT_ENABLE

// =============================================================================
// QMK FEATURES UNUSED BY PRAXIS
// =============================================================================

// -----------------------------------------------------------------------------
#ifdef    AUDIO_ENABLE
# undef   AUDIO_ENABLE
#endif // AUDIO_ENABLE

// -----------------------------------------------------------------------------
#ifdef    BLUETOOTH_ENABLE
# undef   BLUETOOTH_ENABLE
#endif // BLUETOOTH_ENABLE

// -----------------------------------------------------------------------------
#ifdef    COMBO_ENABLE
# undef   COMBO_ENABLE
#endif // COMBO_ENABLE

// -----------------------------------------------------------------------------
#ifdef    COMMAND_ENABLE
# undef   COMMAND_ENABLE
#endif // COMMAND_ENABLE

// -----------------------------------------------------------------------------
#ifdef    CONSOLE_ENABLE
# undef   CONSOLE_ENABLE
#endif // CONSOLE_ENABLE

// -----------------------------------------------------------------------------
#ifdef    DEFERRED_EXEC_ENABLE
# undef   DEFERRED_EXEC_ENABLE
#endif // DEFERRED_EXEC_ENABLE

// -----------------------------------------------------------------------------
#ifdef    DYNAMIC_TAPPING_TERM_ENABLE
# undef   DYNAMIC_TAPPING_TERM_ENABLE
#endif // DYNAMIC_TAPPING_TERM_ENABLE

// -----------------------------------------------------------------------------
#ifdef    LEADER_ENABLE
# undef   LEADER_ENABLE
#endif // LEADER_ENABLE

// -----------------------------------------------------------------------------
#ifdef    MIDI_ENABLE
# undef   MIDI_ENABLE
#endif // MIDI_ENABLE

// -----------------------------------------------------------------------------
#ifndef   NO_ACTION_FUNCTION
# define  NO_ACTION_FUNCTION
#endif // NO_ACTION_FUNCTION

// -----------------------------------------------------------------------------
#ifndef   NO_ACTION_MACRO
# define  NO_ACTION_MACRO
#endif // NO_ACTION_MACRO

// -----------------------------------------------------------------------------
#ifndef   NO_ACTION_ONESHOT
# define  NO_ACTION_ONESHOT
#endif // NO_ACTION_ONESHOT

// -----------------------------------------------------------------------------
#ifndef   NO_DEBUG
# define  NO_DEBUG
#endif // NO_DEBUG

// -----------------------------------------------------------------------------
#ifdef    NKRO_ENABLE
# undef   NKRO_ENABLE
#endif // NKRO_ENABLE

// -----------------------------------------------------------------------------
#ifdef    UNICODE_ENABLE
# undef   UNICODE_ENABLE
#endif // UNICODE_ENABLE

// -----------------------------------------------------------------------------
#ifndef   NO_PRINT
# define  NO_PRINT
#endif // NO_PRINT

// =============================================================================
// QMK RGB FEATURES
// =============================================================================

// -----------------------------------------------------------------------------
#define FORCED_SYNC_THROTTLE_MS 100
#define SPLIT_MAX_CONNECTION_ERRORS 10
#define SPLIT_CONNECTION_CHECK_TIMEOUT 500
#define SPLIT_TRANSACTION_IDS_USER SECONDARY_DISPLAY_HANDLER

// -----------------------------------------------------------------------------
#ifdef RGBLIGHT_ENABLE
# undef RGBLIGHT_EFFECT_BREATHING
# undef RGBLIGHT_EFFECT_RAINBOW_MOOD
# undef RGBLIGHT_EFFECT_RAINBOW_SWIRL
# undef RGBLIGHT_EFFECT_SNAKE
# undef RGBLIGHT_EFFECT_KNIGHT
# undef RGBLIGHT_EFFECT_CHRISTMAS
# undef RGBLIGHT_EFFECT_STATIC_GRADIENT
# undef RGBLIGHT_EFFECT_RGB_TEST
# undef RGBLIGHT_EFFECT_ALTERNATING
# undef RGBLIGHT_EFFECT_TWINKLE
# define RGBLIGHT_LIMIT_VAL 120
# define RGBLIGHT_HUE_STEP 10
# define RGBLIGHT_SAT_STEP 17
# define RGBLIGHT_VAL_STEP 17
#endif
