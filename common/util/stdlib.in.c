/*******************************************************************************
 * PRAXIS STDLIB
 ******************************************************************************/

// -----------------------------------------------------------------------------
#if       USE_STDLIB_FUNCTIONS
# include "stdlib/stdlib.in.c"
#else  // USE_STDLIB_FUNCTIONS
# include "stdlib/custom.in.c"
#endif // USE_STDLIB_FUNCTIONS
