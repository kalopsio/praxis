/*******************************************************************************
 * PRAXIS COMMON TYPES
 ******************************************************************************/
#pragma once

// -----------------------------------------------------------------------------
// A value that holds the timestamp read from timer_read().
// -----------------------------------------------------------------------------
typedef uint16_t qmk_timestamp_t;

// -----------------------------------------------------------------------------
// The operating mode of the keyboard, this is used to determine how the
// keyboard responds to certain key presses; see: px_mode_bit_t
// -----------------------------------------------------------------------------
typedef uint8_t px_mode_t;

// -----------------------------------------------------------------------------
// Bits for controlling the operating mode of the keyboard.
// -----------------------------------------------------------------------------
typedef enum px_mode_bit_t {
  // Whether or not the runes are shifted for the keyboard.
  PX_MODE_RUNE_SHIFT_BIT = (1 << 0),

  // Whether or not the layer is shifted for the keyboard.
  PX_MODE_LAYER_SHIFT_BIT = (1 << 1),

  // Whether or not the keyboard is in review mode.
  PX_MODE_REVIEW_MODE_BIT = (1 << 2),

  // Whether or not the keyboard is holding down the left focus key.
  PX_MODE_LEFT_ZETTA_BIT = (1 << 3),

  // Whether or not the keyboard is holding down the right focus key.
  PX_MODE_RIGHT_ZETTA_BIT = (1 << 4),

  // Whether or not to temporarily show the secondary controller information.
  PX_MODE_SHOW_SECONDARY_INFO_BIT = (1 << 5),

  // The last two bits are reserved for the keyboard submode (see below).
} px_mode_bit_t;

#define PX_MODE_ZETTA_MASK (PX_MODE_LEFT_ZETTA_BIT|PX_MODE_RIGHT_ZETTA_BIT)

// For space-saving reasons, we mix the submode into the normal mode type.
// Technically, it would be best to have the mode fit all of these types as
// bits in the same way that px_mode_bits_t does above, but it's very strange to
// want to access these submodes at the same time, so we just wont support it.
typedef enum px_submode_t {
  // The normal submode, this corresponds to the current layer you're on.
  PX_SUBMODE_NORMAL = 0,

  // A submode of the keyboard where you are preparing to kill a tab or window.
  PX_SUBMODE_KILL,

  // Like the normal submode, except the primary OLED displays the build info.
  PX_SUBMODE_PRIMARY_INFO,

  // Like the normal submode, except the primary OLED displays the debug log.
  PX_SUBMODE_DEBUG,
} px_submode_t;

#define PX_SUBMODE_SHIFT 6
#define PX_SUBMODE_MASK (3 << PX_SUBMODE_SHIFT)

// -----------------------------------------------------------------------------
// The operating mode of the OLED; see: px_oled_states_t
// -----------------------------------------------------------------------------
typedef uint8_t px_oled_state_t;

// -----------------------------------------------------------------------------
// The state of the OLED. This is needed in common to send the state to the
// secondary so that they can agree on what to display.
// -----------------------------------------------------------------------------
typedef enum px_oled_states_t {
  PX_OLED_STATE_INACTIVE,
  PX_OLED_STATE_SPLASH,
  PX_OLED_STATE_ACTIVE,
  PX_OLED_STATE_PENDING_DECREASING_BRIGHTNESS,
  PX_OLED_STATE_DECREASING_BRIGHTNESS,
  PX_OLED_STATE_PENDING_DEACTIVATION
} px_oled_states_t;

// -----------------------------------------------------------------------------
// A type that is sent from the primary keyboard to the secondary. This type
// allows us to instruct the secondary display on how it should update the OLED
// screen (which usually displays statistics).
// -----------------------------------------------------------------------------
typedef struct px_update_display_t {
  // The current mode state of the keyboard.
  px_mode_t mode;

  // The state of the OLED, so that they display an agreed-upon thing.
  px_oled_state_t oled_state;

  // The brightness of the primary display (for handling fade-out).
  // Note: 0 means the display is off entirely.
  uint8_t brightness;

  // A counter for the current second that we are processing for the WPM stats.
  // This message may be sent multiple times per second, so we need a way to
  // differentiate when the primary starts counting a new second.
  uint8_t counter;

  // The current WPM reported by the primary. This value is the WPM over the
  // last minute from the time that the message was sent. It should be used as
  // the WPM for the current second as indexed by `counter`.
  uint16_t wpm;

  // The maximum WPM that the primary has ever seen. This is sent from the
  // primary even though it is not strictly needed to show consistent data when
  // the primary resets but the secondary does not.
  uint16_t max;

  // The number of characters typed total by the keyboard since power-on.
  uint32_t chars;

  // The number of words typed total by the keyboard since power-on.
  uint32_t words;

  // The number of keys typed total by the keyboard since power-on.
  uint32_t keys;
} px_update_display_t;
