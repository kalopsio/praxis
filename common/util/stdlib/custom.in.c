/*******************************************************************************
 * PRAXIS STDLIB (Custom)
 ******************************************************************************/

// -----------------------------------------------------------------------------
static void praxis_memmove_fwd (void const * begin, void const * end, void * dest) {
  uint8_t const * beginb = (uint8_t const *)begin;
  uint8_t const * endb = (uint8_t const *)end;
  uint8_t * destb = (uint8_t *)dest;
  for (; beginb != endb; ++beginb, ++destb) {
    *destb = *beginb;
  }
}

// -----------------------------------------------------------------------------
static void praxis_memmove_rev (void const * rbegin, void const * rend, void * rdest) {
  uint8_t const * rbeginb = (uint8_t const *)rbegin;
  uint8_t const * rendb = (uint8_t const *)rend;
  uint8_t * rdestb = (uint8_t *)rdest;
  for (; rbeginb != rendb; --rbeginb, --rdestb) {
    *rdestb = *rbeginb;
  }
}

// -----------------------------------------------------------------------------
static void praxis_memmove (void * dest, void const * src, size_t bytes) {
  if (src >= dest) {
    praxis_memmove_fwd(src, src + bytes, dest);
  } else {
    praxis_memmove_rev(src + bytes - 1, src - 1, dest + bytes - 1);
  }
}

// -----------------------------------------------------------------------------
static void praxis_memzero (void * ptr, size_t bytes) {
  uint8_t * ptrb = (uint8_t *)ptr;
  while (bytes--) {
    *ptrb = 0;
    ++ptrb;
  }
}

// -----------------------------------------------------------------------------
static int praxis_memcmp (const void * lhs, const void * rhs, size_t bytes) {
  const uint8_t * lhsb = (const uint8_t *)lhs;
  const uint8_t * rhsb = (const uint8_t *)rhs;
  while (bytes) {
    const int diff = *rhsb - *lhsb;
    if (diff) {
      return diff;
    }
    ++lhsb;
    ++rhsb;
    --bytes;
  }
  return 0;
}
