/*******************************************************************************
 * PRAXIS STDLIB (Stdlib)
 ******************************************************************************/

#include <string.h>

// -----------------------------------------------------------------------------
static void praxis_memmove (void * dest, void const * src, size_t bytes) {
  (void)memmove(dest, src, bytes);
}

// -----------------------------------------------------------------------------
static void praxis_memzero (void * ptr, size_t bytes) {
  (void)memset(ptr, '\0', bytes);
}

// -----------------------------------------------------------------------------
static int praxis_memcmp (const void * lhs, const void * rhs, size_t bytes) {
  return memcmp(lhs, rhs, bytes);
}
