/*******************************************************************************
 * PRAXIS OLED (Common)
 ******************************************************************************/

// =============================================================================
// Macros
// =============================================================================

// -----------------------------------------------------------------------------
#define BIT__ 0
#define BIT_X 1

// -----------------------------------------------------------------------------
// This macro forms a vertical line where the top is c00, and the bottom is c07.
// -----------------------------------------------------------------------------
#define OLED_1x8(c00, c01, c02, c03, c04, c05, c06, c07)                        \
  (((c07) << 7) | ((c06) << 6) | ((c05) << 5) | ((c04) << 4)                    \
  |((c03) << 3) | ((c02) << 2) | ((c01) << 1) | ((c00) << 0))

// -----------------------------------------------------------------------------
// This macro forms a 3x5 digit, with 2 spaces of padding on the top (3x8).
// Note that this places the lines backwards, that way we can reverse iterate.
// -----------------------------------------------------------------------------
#define OLED_DIGIT(                                                             \
  c00, c01, c02,                                                                \
  c10, c11, c12,                                                                \
  c20, c21, c22,                                                                \
  c30, c31, c32,                                                                \
  c40, c41, c42                                                                 \
)                                                                               \
 OLED_1x8(0, 0, BIT_##c02, BIT_##c12, BIT_##c22, BIT_##c32, BIT_##c42, 0),      \
 OLED_1x8(0, 0, BIT_##c01, BIT_##c11, BIT_##c21, BIT_##c31, BIT_##c41, 0),      \
 OLED_1x8(0, 0, BIT_##c00, BIT_##c10, BIT_##c20, BIT_##c30, BIT_##c40, 0)

// =============================================================================
// Bitmaps
// =============================================================================

// -----------------------------------------------------------------------------
// 'numbers', 30x8px
// -----------------------------------------------------------------------------
static const char epd_bitmap_numbers [] = {
    // 0
    OLED_DIGIT(
      X,X,X,
      X,_,X,
      X,_,X,
      X,_,X,
      X,X,X
    ),
    // 1
    OLED_DIGIT(
      _,X,_,
      _,X,_,
      _,X,_,
      _,X,_,
      _,X,_
    ),
    // 2
    OLED_DIGIT(
      X,X,X,
      _,_,X,
      X,X,X,
      X,_,_,
      X,X,X
    ),
    // 3
    OLED_DIGIT(
      X,X,X,
      _,_,X,
      X,X,X,
      _,_,X,
      X,X,X
    ),
    // 4
    OLED_DIGIT(
      X,_,X,
      X,_,X,
      X,X,X,
      _,_,X,
      _,_,X
    ),
    // 5
    OLED_DIGIT(
      X,X,X,
      X,_,_,
      X,X,X,
      _,_,X,
      X,X,X
    ),
    // 6
    OLED_DIGIT(
      X,X,X,
      X,_,_,
      X,X,X,
      X,_,X,
      X,X,X
    ),
    // 7
    OLED_DIGIT(
      X,X,X,
      _,_,X,
      _,_,X,
      _,_,X,
      _,_,X
    ),
    // 8
    OLED_DIGIT(
      X,X,X,
      X,_,X,
      X,X,X,
      X,_,X,
      X,X,X
    ),
    // 9
    OLED_DIGIT(
      X,X,X,
      X,_,X,
      X,X,X,
      _,_,X,
      X,X,X
    )
};

// =============================================================================
// Globals
// =============================================================================

// -----------------------------------------------------------------------------
static uint8_t s_oled_line = 0;

// =============================================================================
// Helper Functions
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_oled_set_line (uint8_t line) {
  s_oled_line = line;
  oled_set_cursor(0, line);
}

// -----------------------------------------------------------------------------
static void praxis_oled_reset_line (void) {
  praxis_oled_set_line(0);
}

// -----------------------------------------------------------------------------
static void praxis_oled_advance_line (void) {
  praxis_oled_set_line(s_oled_line + 1);
}

// -----------------------------------------------------------------------------
static void praxis_oled_draw_line_P (const char bitmap[32]) {
  oled_write_raw_P(bitmap, 32);
  praxis_oled_advance_line();
}

// -----------------------------------------------------------------------------
// Draws the number provided to the output buffer (must be 32-width).
// -----------------------------------------------------------------------------
static void write_numbers_to_buffer (
  uint32_t value,
  uint8_t left_bounds,
  uint8_t right_bounds,
  char* output
) {
  int8_t position = 32 - right_bounds;
  do {
    uint8_t const digit = value % 10;
    uint8_t digit_offset = digit*3;
    value /= 10;
    output[position--] |= epd_bitmap_numbers[digit_offset++];
    output[position--] |= epd_bitmap_numbers[digit_offset++];
    output[position--] |= epd_bitmap_numbers[digit_offset++];
    position--;
  } while (value > 0 && position > left_bounds);
}

// -----------------------------------------------------------------------------
// Draws numbers to the screen by first placing them in a blank buffer.
// -----------------------------------------------------------------------------
static void draw_numbers_to_blank (uint32_t value) {
    char output[32] = {};
    write_numbers_to_buffer(value, 2, 2, output);
    oled_write_raw(output, sizeof(output));
}
