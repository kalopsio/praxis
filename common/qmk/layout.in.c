/*******************************************************************************
 * PRAXIS QMK LAYOUT
 ******************************************************************************/

// -----------------------------------------------------------------------------
// The reason this definition is in another file is to discourage editing it.
//
// This key layout translates from qmk_keycode_t to px_keycode_t, you normally
// should not need to edit this file. If you want to change what all of the keys
// do, then you should edit the primary PX layout instead of this file.
// -----------------------------------------------------------------------------
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [0] = LAYOUT_split_3x5_3(
  /*┍━━━━━━━━━━━━━┯━━━━━━━━━━━━━┯━━━━━━━━━━━━━┯━━━━━━━━━━━━━┯━━━━━━━━━━━━━┑     ┍━━━━━━━━━━━━━┯━━━━━━━━━━━━━┯━━━━━━━━━━━━━┯━━━━━━━━━━━━━┯━━━━━━━━━━━━━┑*/
  /*│             │             │             │             │             │     │             │             │             │             │             │*/
  /*│*/ PX_0x0, /*│*/ PX_0x1, /*│*/ PX_0x2, /*│*/ PX_0x3, /*│*/ PX_0x4, /*│     │*/ PX_0x5, /*│*/ PX_0x6, /*│*/ PX_0x7, /*│*/ PX_0x8, /*│*/ PX_0x9, /*│*/
  /*├─────────────┼─────────────┼─────────────┼─────────────┼─────────────|     |─────────────┼─────────────┼─────────────┼─────────────┼─────────────|*/
  /*│             │             │             │             │             │     │             │             │             │             │             │*/
  /*│*/ PX_1x0, /*│*/ PX_1x1, /*│*/ PX_1x2, /*│*/ PX_1x3, /*│*/ PX_1x4, /*│     │*/ PX_1x5, /*│*/ PX_1x6, /*│*/ PX_1x7, /*│*/ PX_1x8, /*│*/ PX_1x9, /*│*/
  /*├─────────────┼─────────────┼─────────────┼─────────────┼─────────────|     |─────────────┼─────────────┼─────────────┼─────────────┼─────────────|*/
  /*│             │             │             │             │             │     │             │             │             │             │             │*/
  /*│*/ PX_2x0, /*│*/ PX_2x1, /*│*/ PX_2x2, /*│*/ PX_2x3, /*│*/ PX_2x4, /*│     │*/ PX_2x5, /*│*/ PX_2x6, /*│*/ PX_2x7, /*│*/ PX_2x8, /*│*/ PX_2x9, /*│*/
  /*╰─────────────┴─────────────┼─────────────┼─────────────┼─────────────|     |─────────────┼─────────────┼─────────────┼─────────────┴─────────────╯*/
  /*                            │             │             │             │     │             │             │             │                            */
  /*                            │*/ PX_3x0, /*│*/ PX_3x1, /*│*/ PX_3x2, /*│     │*/ PX_3x3, /*│*/ PX_3x4 ,/*│*/ PX_3x5  /*│                            */
  /*                            ╰─────────────┴─────────────┴─────────────╯     ╰─────────────┴─────────────┴─────────────╯                            */
  )
};
