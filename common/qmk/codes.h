/*******************************************************************************
 * PRAXIS QMK CODES
 ******************************************************************************/
#pragma once

#include "../util/macros.h"

// =============================================================================
// Types
// =============================================================================

// -----------------------------------------------------------------------------
// The underlying type for px_keycode_enum_t values - use this instead of the
// enum typename to ensure proper bit depth on the fields.
// -----------------------------------------------------------------------------
typedef uint16_t px_keycode_t;

// -----------------------------------------------------------------------------
// If you want to specify exact kind of keyrange required for a function, you
// can use these typedefs. They won't cause compile failure on misuse, but they
// will make the code easier to read.
// -----------------------------------------------------------------------------
typedef px_keycode_t px_action_keycode_t;
typedef px_keycode_t px_combo_keycode_t;
typedef px_keycode_t px_hw_keycode_t;

// -----------------------------------------------------------------------------
// The actual keycode values. See the different sections for the enum values to
// see how these keycodes are used. We encode metadata about the ranges of keys
// within the enum to keep the definition of the ranges close to the definition
// of the keys (these are marked with /*Metadata:*/ comments to clarify).
//
// Range format:
//   *_BEGIN = First key in the range.
//   *_LAST  = Last key in the range.
//   *_END   = One past the last key in the range (for consistent for-loops).
// -----------------------------------------------------------------------------
enum px_keycode_enum_t {
  /*Metadata:*/ PX_KEYS_BEGIN = QK_USER,

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Hardware Key Codes
  //   The hardware keys that Praxis uses on the actual QMK keymap. These keys
  //   are low-level keys which should usually not be used directly. Still, the
  //   definition of these keys is important to have so that we can implement
  //   our own logic for things like layer switching, mouse handling, etc.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  /*Metadata:*/ PX_HW_KEYS_BEGIN = PX_KEYS_BEGIN,
  PX_0x0 = PX_HW_KEYS_BEGIN,
  /*┍━━━━━━━━━━━━━┯━━━━━━━━━━━━━┯━━━━━━━━━━━━━┯━━━━━━━━━━━━━┯━━━━━━━━━━━━━┑     ┍━━━━━━━━━━━━━┯━━━━━━━━━━━━━┯━━━━━━━━━━━━━┯━━━━━━━━━━━━━┯━━━━━━━━━━━━━┑*/
  /*│             │             │             │             │             │     │             │             │             │             │             │*/
  /*│   PX_0x0    │*/ PX_0x1, /*│*/ PX_0x2, /*│*/ PX_0x3, /*│*/ PX_0x4, /*│     │*/ PX_0x5, /*│*/ PX_0x6, /*│*/ PX_0x7, /*│*/ PX_0x8, /*│*/ PX_0x9, /*│*/
  /*├─────────────┼─────────────┼─────────────┼─────────────┼─────────────|     |─────────────┼─────────────┼─────────────┼─────────────┼─────────────|*/
  /*│             │             │             │             │             │     │             │             │             │             │             │*/
  /*│*/ PX_1x0, /*│*/ PX_1x1, /*│*/ PX_1x2, /*│*/ PX_1x3, /*│*/ PX_1x4, /*│     │*/ PX_1x5, /*│*/ PX_1x6, /*│*/ PX_1x7, /*│*/ PX_1x8, /*│*/ PX_1x9, /*│*/
  /*├─────────────┼─────────────┼─────────────┼─────────────┼─────────────|     |─────────────┼─────────────┼─────────────┼─────────────┼─────────────|*/
  /*│             │             │             │             │             │     │             │             │             │             │             │*/
  /*│*/ PX_2x0, /*│*/ PX_2x1, /*│*/ PX_2x2, /*│*/ PX_2x3, /*│*/ PX_2x4, /*│     │*/ PX_2x5, /*│*/ PX_2x6, /*│*/ PX_2x7, /*│*/ PX_2x8, /*│*/ PX_2x9, /*│*/
  /*╰─────────────┴─────────────┼─────────────┼─────────────┼─────────────|     |─────────────┼─────────────┼─────────────┼─────────────┴─────────────╯*/
  /*                            │             │             │             │     │             │             │             │                            */
  /*                            │*/ PX_3x0, /*│*/ PX_3x1, /*│*/ PX_3x2, /*│     │*/ PX_3x3, /*│*/ PX_3x4, /*│*/ PX_3x5, /*│                            */
  /*                            ╰─────────────┴─────────────┴─────────────╯     ╰─────────────┴─────────────┴─────────────╯                            */
  /*Metadata:*/ PX_HW_KEYS_END,
  /*Metadata:*/ PX_HW_KEYS_LAST = PX_HW_KEYS_END - 1,

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Action Key Codes
  //   These key codes correspond to non-standard actions, and may be used for
  //   Registering a key to do something other than the normal behaviour of
  //   sending a keycode, shifting a layer, etc.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  /*Metadata:*/ PX_ACTION_KEYS_BEGIN = PX_HW_KEYS_END,

  // While holding down this key, you can press one of the big inner thumb keys
  // to send the kill command for the current tab/window. Any other key presses
  // while holding this key action down is invalid and will be ignored.
  PX_ACTION_KILL_MODE_TOGGLE = PX_ACTION_KEYS_BEGIN,

  PX_ACTION_PREVIOUS_WINDOW,
  PX_ACTION_NEXT_WINDOW,

  /*Metadata:*/ PX_ACTION_KEYS_END,
  /*Metadata:*/ PX_ACTION_KEYS_LAST = PX_ACTION_KEYS_END - 1,

  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  // Combo Keys
  //   These key codes correspond to combo actions, which are actions that
  //   activate based on pressing more than one key at (or near) the same time.
  // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  /*Metadata:*/ PX_COMBO_KEYS_BEGIN = PX_ACTION_KEYS_END,

  PX_COMBO_MODE_SWITCH_EDIT = PX_COMBO_KEYS_BEGIN,
  PX_COMBO_MODE_SWITCH_REVIEW,
  PX_COMBO_MODE_TOGGLE,
  PX_COMBO_DEBUG_PRIMARY_INFO,
  PX_COMBO_DEBUG_SECONDARY_INFO,
  PX_COMBO_DEBUG_EVENT_LOG,
  PX_COMBO_DEBUG_STATS_RESET,
  PX_COMBO_MACRO_CURLY,
  PX_COMBO_MACRO_PARENS,
  PX_COMBO_MACRO_SQUARE,
  PX_COMBO_MACRO_ANGLE,
  PX_COMBO_MACRO_REGEX,

  /*Metadata:*/ PX_COMBO_KEYS_END,
  /*Metadata:*/ PX_COMBO_KEYS_LAST = PX_COMBO_KEYS_END - 1,
  /*Metadata:*/ PX_KEYS_END = PX_COMBO_KEYS_END,
  /*Metadata:*/ PX_KEYS_LAST = PX_COMBO_KEYS_LAST,
};

// =============================================================================
// Key Metadata
// =============================================================================

// -----------------------------------------------------------------------------
// Sizes
// -----------------------------------------------------------------------------
#define PX_ACTION_KEYS_COUNT            (PX_ACTION_KEYS_END - PX_ACTION_KEYS_BEGIN)
#define PX_COMBO_KEYS_COUNT             (PX_COMBO_KEYS_END - PX_COMBO_KEYS_BEGIN)
#define PX_HW_KEYS_COUNT                (PX_HW_KEYS_END - PX_HW_KEYS_BEGIN)
#define PX_KEYS_COUNT                   (PX_KEYS_END - PX_KEYS_BEGIN)

// -----------------------------------------------------------------------------
// Shift Keys
// -----------------------------------------------------------------------------
#define PX_LAYER_SHIFT                  PX_3x1
#define PX_RUNE_SHIFT                   PX_3x4

// -----------------------------------------------------------------------------
// Thumb Keys
// -----------------------------------------------------------------------------
#define PX_LEFT_THUMB_OUTER             PX_3x0
#define PX_LEFT_THUMB_HOME              PX_3x1
#define PX_LEFT_THUMB_INNER             PX_3x2
#define PX_RIGHT_THUMB_INNER            PX_3x3
#define PX_RIGHT_THUMB_HOME             PX_3x4
#define PX_RIGHT_THUMB_OUTER            PX_3x5

// -----------------------------------------------------------------------------
// Thumb Ranges
// -----------------------------------------------------------------------------
#define PX_LEFT_THUMB_BEGIN             PX_LEFT_THUMB_OUTER
#define PX_LEFT_THUMB_LAST              PX_LEFT_THUMB_INNER
#define PX_LEFT_THUMB_END               (PX_LEFT_THUMB_INNER + 1)
#define PX_RIGHT_THUMB_BEGIN            PX_RIGHT_THUMB_OUTER
#define PX_RIGHT_THUMB_LAST             PX_RIGHT_THUMB_INNER
#define PX_RIGHT_THUMB_END              (PX_RIGHT_THUMB_INNER + 1)
#define PX_THUMB_BEGIN                  PX_LEFT_THUMB_BEGIN
#define PX_THUMB_LAST                   PX_RIGHT_THUMB_LAST
#define PX_THUMB_END                    PX_RIGHT_THUMB_END

// -----------------------------------------------------------------------------
// Modifier Keys
// -----------------------------------------------------------------------------
#define PX_LEFT_ZETTA                   PX_1x0
#define PX_LEFT_CONTROL                 PX_2x1
#define PX_LEFT_ALT                     PX_2x2
#define PX_LEFT_GUI                     PX_2x3
#define PX_RIGHT_GUI                    PX_2x6
#define PX_RIGHT_ALT                    PX_2x7
#define PX_RIGHT_CONTROL                PX_2x8
#define PX_RIGHT_ZETTA                  PX_1x9

// -----------------------------------------------------------------------------
// Modifier Ranges
// -----------------------------------------------------------------------------
#define PX_LEFT_MODIFIER_BEGIN          PX_LEFT_CONTROL
#define PX_LEFT_MODIFIER_LAST           PX_LEFT_GUI
#define PX_LEFT_MODIFIER_END            (PX_LEFT_GUI + 1)
#define PX_RIGHT_MODIFIER_BEGIN         PX_RIGHT_GUI
#define PX_RIGHT_MODIFIER_LAST          PX_RIGHT_CONTROL
#define PX_RIGHT_MODIFIER_END           (PX_RIGHT_CONTROL + 1)

// -----------------------------------------------------------------------------
// Key Checks (Key Ranges)
// -----------------------------------------------------------------------------
#define IS_ACTION_KEY(keycode)          IN_RANGE(keycode, PX_ACTION_KEYS_BEGIN, PX_ACTION_KEYS_LAST)
#define IS_COMBO_KEY(keycode)           IN_RANGE(keycode, PX_COMBO_KEYS_BEGIN, PX_COMBO_KEYS_LAST)
#define IS_HW_KEY(keycode)              IN_RANGE(keycode, PX_HW_KEYS_BEGIN, PX_HW_KEYS_LAST)
#define IS_PX_KEY(keycode)              IN_RANGE(keycode, PX_KEYS_BEGIN, PX_KEYS_LAST)

// -----------------------------------------------------------------------------
// Key Checks (Modifiers)
// -----------------------------------------------------------------------------
#define IS_LEFT_MODIFIER_KEY(keycode)   (IN_RANGE(keycode, PX_LEFT_MODIFIER_BEGIN, PX_LEFT_MODIFIER_LAST) || (keycode == PX_LEFT_ZETTA))
#define IS_RIGHT_MODIFIER_KEY(keycode)  (IN_RANGE(keycode, PX_RIGHT_MODIFIER_BEGIN, PX_RIGHT_MODIFIER_LAST) || (keycode == PX_RIGHT_ZETTA))
#define IS_MODIFIER_KEY(keycode)        (IS_LEFT_MODIFIER_KEY(keycode) || IS_RIGHT_MODIFIER_KEY(keycode))

// -----------------------------------------------------------------------------
// Key Checks (Shift)
// -----------------------------------------------------------------------------
#define IS_LEFT_SHIFT_KEY(keycode)      ((keycode) == PX_LAYER_SHIFT)
#define IS_RIGHT_SHIFT_KEY(keycode)     ((keycode) == PX_RUNE_SHIFT)
#define IS_SHIFT_KEY(keycode)           (IS_LEFT_SHIFT_KEY(keycode) || IS_RIGHT_SHIFT_KEY(keycode))

// -----------------------------------------------------------------------------
// Key Checks (Thumb)
// -----------------------------------------------------------------------------
#define IS_SIDE_THUMB_KEY(keycode)      (IS_THUMB_KEY(keycode) && !IS_HOME_THUMB_KEY(keycode))
#define IS_HOME_THUMB_KEY(keycode)      ((keycode) == PX_LEFT_THUMB_HOME || (keycode) == PX_RIGHT_THUMB_HOME)
#define IS_LEFT_THUMB_KEY(keycode)      IN_RANGE(keycode, PX_LEFT_THUMB_BEGIN, PX_LEFT_THUMB_LAST)
#define IS_RIGHT_THUMB_KEY(keycode)     IN_RANGE(keycode, PX_RIGHT_THUMB_BEGIN, PX_RIGHT_THUMB_LAST)
#define IS_THUMB_KEY(keycode)           IN_RANGE(keycode, PX_THUMB_BEGIN, PX_THUMB_LAST)

// -----------------------------------------------------------------------------
// Key Checks (Other)
// -----------------------------------------------------------------------------
#define IS_FUNCTION_KEY(keycode)        ((keycode) >= KC_F1 && (keycode) <= KC_F12)

// =============================================================================
// Static Asserts
// =============================================================================

// -----------------------------------------------------------------------------
// We're using the QMK User key feature to support this keyboard. So we have to
// ensure that we aren't defining more user keys than what QMK supports.
// -----------------------------------------------------------------------------
_Static_assert(
  (uint16_t)PX_KEYS_END < (uint16_t)QK_USER_MAX,
  "Too many Praxis keys have been defined (overflow outside of user keys)."
);
