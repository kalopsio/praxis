/*******************************************************************************
 * PRAXIS KEYMAP
 *   Compile with `clear && qmk compile -kb crkbd -km praxis && qmk flash`.
 ******************************************************************************/

#include QMK_KEYBOARD_H
#include "transactions.h"

// =============================================================================
// PRAXIS DEBUG OPTIONS
// =============================================================================

// -----------------------------------------------------------------------------
// ENABLE_DEBUG
//   When you comment this in, it enables debugging across the whole keyboard.
//   Please be aware, this usually results in a keyboard that types out the
//   internal debugging details to whatever window has your focus. This means
//   that a debug-mode Praxis keyboard is usually functionally useless!
//
//   Nevertheless, typing out debugging information is often useful for quick
//   printf-style debugging of the internal workings of the keyboard.
// -----------------------------------------------------------------------------
// #define ENABLE_DEBUG

// =============================================================================
// PRAXIS CONFIGURATION OPTIONS
// =============================================================================

// -----------------------------------------------------------------------------
// ENABLE_RGB
//   Whether or not we will enable and show RBG lighting on the keyboard.
// -----------------------------------------------------------------------------
// #define ENABLE_RGB

// -----------------------------------------------------------------------------
// ENABLE_POINTER_JERKS
//   Whether or not to allow the processing of "jerks" for the pointer velocity.
//   In general, you should avoid needing this. If you need this, it's because
//   you've done something to introduce a delay on pressing mouse keys and them
//   being processed. But if you absolutely need this, it has been coded.
// -----------------------------------------------------------------------------
// #define ENABLE_POINTER_JERKS

// -----------------------------------------------------------------------------
// USE_STDLIB_FUNCTIONS (0/1)
//   Whether or not to use stdlib library functions when we have the option.
//   Examples include `memmove`, and `memset`, though it may include other such
//   functions as well.
// -----------------------------------------------------------------------------
#define USE_STDLIB_FUNCTIONS            0

// -----------------------------------------------------------------------------
// COMBO_GROUPING_TERM (ms)
//   Defines how long it takes for keys to be joined together with contiguous
//   other key presses. If you press keys within this time delta, Praxis will
//   attempt to treat them as one key press (if that corresponds to something).
//   If the combo of keys does not pertain to an action, it is treated as a
//   sequence of single key presses.
//
// RECOMMENDATIONS
//   If you find that your combo keys aren't being handled as you expect, then
//   you may need to increase this value.
//
// NOTE
//   The layout.in.c file also defines which keys are allowed to be processed
//   for combos. If a key is supposed to be a part of a combo, but it's not
//   marked for COMBOS, then it will not wait for the grouping term.
// -----------------------------------------------------------------------------
#define COMBO_GROUPING_TERM             75

// -----------------------------------------------------------------------------
// HOLDING_TERM (ms)
//   Defines how long it takes for a key to be processed as a hold instead of a
//   press. Some keys react differently when held (for example, the modifier
//   keys).
//
// RECOMMENDATIONS
//   If you find that you are accidentally holding down modifier keys while you
//   type, you may need to increase this value.
//
// REQUIREMENTS
//   The holding term must be a value larger than the grouping term.
// -----------------------------------------------------------------------------
#define HOLDING_TERM                    250

// -----------------------------------------------------------------------------
_Static_assert(
  HOLDING_TERM > COMBO_GROUPING_TERM,
  "The holding term must be larger than the grouping term."
);

// -----------------------------------------------------------------------------
// SHIFT_RELEASE_TERM (ms)
//   Defines how long it takes past releasing the physical shift keys for the
//   shift keys to actually be logically released. This can help for fast typers
//   who release the shift key before pressing the key they intended to shift.
//
// RECOMMENDATIONS
//   If you find that you are accidentally holding shift for an additional
//   input, then you may need to decrease this value. Likewise, if you find that
//   you are typing unshifted characters when you release the shift key, then
//   you may need to increase this value.
// -----------------------------------------------------------------------------
#define SHIFT_RELEASE_TERM              15

// -----------------------------------------------------------------------------
// DOUBLE_TAP_TERM (ms)
//   Defines how long it takes for a key to be processed as a double-tap. When
//   keys are double-tapped they may take some different actions than if you
//   just single-tapped and held them. Right now, this is only used for allowing
//   users to "hold" the key under a mod key (by double-tapping).
//
// RECOMMENDATIONS
//   If you find that you cannot hold the key under a modifier key by double-tap
//   action, then you may need to increase this value.
// -----------------------------------------------------------------------------
#define DOUBLE_TAP_TERM                 125

// -----------------------------------------------------------------------------
// MAX_KEYS_HELD (count)
//   Defines how many keys are able to be held at the same time. Any more keys
//   than this will be swallowed. This defines some static arrays in the code
//   for tracking key state machines.
//
// RECOMMENDATIONS
//   If you find that your key presses are being swallowed, then you may want to
//   increase this value.
// -----------------------------------------------------------------------------
#define MAX_KEYS_HELD                   10

// -----------------------------------------------------------------------------
// MOUSE_SPEED_{START,END} (px/sec)
//   Defines how fast the mouse will travel at the start of input, and then how
//   fast it will be going after holding the mouse key for one second. These
//   values are in pixels per second, so it sounds fast but if you set this too
//   slow it's cumbersome to use the mouse keys.
// -----------------------------------------------------------------------------
#define MOUSE_SPEED_START               (125.0f * 2)
#define MOUSE_SPEED_END                 (2500.0f * 2)

// -----------------------------------------------------------------------------
// MOUSE_WHEEL_DELAY (ms)
//   Defines how long of a pause there is between pressing a mousewheel button
//   and processing continuous movement. Each single button press results in a
//   single wheel scroll occurring, followed by this pause, followed by the
//   continuous movement defined by MOUSE_WHEEL_SPEED_{START,END}.
//
// RECOMMENDATIONS
//   If you find that you have trouble with fine movement using the mouse wheel,
//   then you may wish to increase this.
// -----------------------------------------------------------------------------
#define MOUSE_WHEEL_DELAY               125

// -----------------------------------------------------------------------------
// MOUSE_WHEEL_SPEED_{START,END} (tick/sec)
//   Defines how fast the mouse wheel moves at the start of input, and then how
//   fast it will be going after holding the mouse key for one second. These
//   values are in "ticks" per second, a single tick doesn't always result in a
//   single line scroll.
// -----------------------------------------------------------------------------
#define MOUSE_WHEEL_SPEED_START         50.0f
#define MOUSE_WHEEL_SPEED_END           350.0f

// -----------------------------------------------------------------------------
// OLED_{DEACTIVATE,FADE,IDLE,*}_TERM (ms)
//   Defines how long it takes for the OLED to decide to fade, then the fadeout,
//   then how long to wait at the lowest brightness before turing the screen off
//   entirely. You can see how this is implemented in the oled code.
// -----------------------------------------------------------------------------
#define OLED_DEACTIVATE_TERM            1000
#define OLED_FADE_TERM                  1000
#define OLED_IDLE_TERM                  30000
#define OLED_SECONDARY_DISCONNECT_TERM  5000
#define OLED_SECONDARY_UPDATE_TERM      1000
#define OLED_SPLASH_TERM                5000

// =============================================================================
// PRAXIS INCLUDES (UNITY BUILD)
// =============================================================================

// -----------------------------------------------------------------------------
// Common Includes
// -----------------------------------------------------------------------------
#include "common/meta/hash.h"
#include "common/meta/revision.h"
#include "common/meta/target.h"
#include "common/meta/targets.h"
#include "common/qmk/codes.h"
#include "common/util/macros.h"
#include "common/util/types.h"

// -----------------------------------------------------------------------------
// Common Definitions
// -----------------------------------------------------------------------------
#include "common/qmk/layout.in.c"
#include "common/util/oled.in.c"
#include "common/util/stdlib.in.c"

// -----------------------------------------------------------------------------
// Primary Keyboard Includes
// -----------------------------------------------------------------------------
#if PRAXIS_TARGET == PRAXIS_PRIMARY
# include "primary/source.in.c"
#else
# include "secondary/source.in.c"
#endif

// =============================================================================
// PRAXIS QMK FUNCTION REGISTRATION
// =============================================================================

// -----------------------------------------------------------------------------
void keyboard_post_init_user (void) {
  praxis_handle_init();
}

// -----------------------------------------------------------------------------
bool process_record_user (px_keycode_t keycode, keyrecord_t* record) {
  praxis_handle_event(keycode, record);
  return false;
}

// -----------------------------------------------------------------------------
void housekeeping_task_user (void) {
  praxis_handle_update();
}

// -----------------------------------------------------------------------------
void pointing_device_driver_init (void) {}

// -----------------------------------------------------------------------------
// NOTE: The buttons will remain sticky in the mouse_report,
//       but all the rest of the other data will not.
// -----------------------------------------------------------------------------
report_mouse_t pointing_device_driver_get_report (report_mouse_t mouse_report) {
  praxis_handle_pointer(&mouse_report);
  return mouse_report;
}

// -----------------------------------------------------------------------------
uint16_t pointing_device_driver_get_cpi (void) {
  return 0;
}

// -----------------------------------------------------------------------------
void pointing_device_driver_set_cpi (uint16_t cpi) {}

// -----------------------------------------------------------------------------
oled_rotation_t oled_init_user (oled_rotation_t rotation) {
  return OLED_ROTATION_270;
}

// -----------------------------------------------------------------------------
bool oled_task_user (void) {
  praxis_handle_oled();
  return false;
}
