/*******************************************************************************
 * PRAXIS EVENT HANDLERS (UPDATE)
 ******************************************************************************/

// -----------------------------------------------------------------------------
static void praxis_update_oled (void) {
  static qmk_timestamp_t s_last_update = 0;
  static uint8_t s_last_counter = 0;
  if (s_stats.oled_state == PX_OLED_STATE_INACTIVE) {
    return;
  }
  if (s_last_update == 0 || s_last_counter != s_stats.counter) {
    s_last_update = timer_read();
    s_last_counter = s_stats.counter;
    return;
  }
  if (timer_elapsed(s_last_update) < OLED_SECONDARY_DISCONNECT_TERM) {
    // Disconnect term hasn't passed, wait longer.
    return;
  }

  // We have gone more than the disconnect term for an update to the secondary.
  // Somehow we have disconnected from the primary, we should turn off the OLED.
  s_stats.brightness = 0;
  s_stats.oled_state = PX_OLED_STATE_INACTIVE;
  oled_set_brightness(0);
  oled_off();
}

// -----------------------------------------------------------------------------
static void praxis_handle_update (void) {
  praxis_update_oled();
}
