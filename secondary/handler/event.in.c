/*******************************************************************************
 * PRAXIS EVENT HANDLERS (KEY EVENT)
 ******************************************************************************/

// -----------------------------------------------------------------------------
static void praxis_handle_event (px_keycode_t keycode, keyrecord_t* record) {
  // Secondary does not handle any events.
}
