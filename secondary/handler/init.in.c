/*******************************************************************************
 * PRAXIS EVENT HANDLERS (INITIALIZATION)
 ******************************************************************************/

// -----------------------------------------------------------------------------
void secondary_display_handler (uint8_t in_buflen, const void* in_data, uint8_t out_buflen, void* out_data) {
  const px_update_display_t *update = (const px_update_display_t*)in_data;
  if ((update->keys == 0) && (s_stats.keys != 0)) {
    praxis_memzero(&s_stats.wpm_samples, sizeof(s_stats.wpm_samples));
    s_last_error = PX_ERROR_NONE;
  }
  if (s_stats.counter != update->counter) {
    s_stats.counter = update->counter;
    praxis_memmove(&s_stats.wpm_samples[1], &s_stats.wpm_samples[0], sizeof(uint16_t) * (32 - 1));
  }
  s_stats.oled_state = update->oled_state;
  s_stats.brightness = update->brightness;
  s_stats.max_wpm = update->max;
  s_stats.wpm_samples[0] = update->wpm;
  s_stats.chars = update->chars;
  s_stats.words = update->words;
  s_stats.mode = update->mode;
  s_stats.keys = update->keys;
}

// -----------------------------------------------------------------------------
static void praxis_handle_init (void) {
  transaction_register_rpc(SECONDARY_DISPLAY_HANDLER, secondary_display_handler);
#ifdef    RGBLIGHT_ENABLE
#ifdef    ENABLE_RGB
  rgblight_enable_noeeprom();
  rgblight_sethsv_noeeprom(180, 255, 255);
  rgblight_mode_noeeprom(RGBLIGHT_MODE_BREATHING);
#else  // ENABLE_RGB
  rgblight_disable_noeeprom();
#endif // ENABLE_RGB
#endif // RGBLIGHT_ENABLE
}
