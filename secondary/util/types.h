/*******************************************************************************
 * PRAXIS TYPES
 ******************************************************************************/
#pragma once

// =============================================================================
// General Types
// =============================================================================

typedef enum px_error_t {
  // A default value for the error enum, indicates no error.
  PX_ERROR_NONE,
} px_error_t;

// A type for holding all of the statistics that we display on the OLED.
typedef struct px_oled_stats_t {
  px_mode_t mode;
  px_oled_state_t oled_state;
  uint8_t brightness;
  uint8_t counter;
  uint16_t max_wpm;
  uint32_t chars;
  uint32_t words;
  uint32_t keys;
  uint16_t wpm_samples[32];
  px_error_t error;
} px_oled_stats_t;
