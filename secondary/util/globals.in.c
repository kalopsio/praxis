/*******************************************************************************
 * PRAXIS GLOBALS
 ******************************************************************************/

// The statistic for the controller.
static px_oled_stats_t s_stats = {
  .oled_state = PX_OLED_STATE_SPLASH,
};

// The last error to occur on the secondary microcontroller.
static px_error_t s_last_error = PX_ERROR_NONE;
