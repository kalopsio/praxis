#!/bin/bash
# ==============================================================================
# flash.sh
# See --help for details, though the script is fairly straightforward.
# ==============================================================================
set -eufo pipefail

# ==============================================================================
# CONSTANTS
# ==============================================================================

# ------------------------------------------------------------------------------
readonly SCRIPTFILE="$(realpath "$0")";
readonly SCRIPTPATH="$(dirname "${SCRIPTFILE}")";

# ------------------------------------------------------------------------------
readonly SOURCE_DIR="${SCRIPTPATH}"
readonly REVISION_FILE="${SOURCE_DIR}/meta/revision"
readonly HASH_FILE="${SOURCE_DIR}/meta/hash"

# ------------------------------------------------------------------------------
readonly LAST_KNOWN_REVISION=$(< ${REVISION_FILE})
readonly LAST_KNOWN_HASH=$(< ${HASH_FILE})

# ==============================================================================
# CLI PARSING
# ==============================================================================

# ------------------------------------------------------------------------------
FLASH=true
HELP=false
REVISION=true
TARGET=both

# ------------------------------------------------------------------------------
for arg in "$@"; do
  shift
  case "$arg" in
    '--both')         TARGET=both;;
    '--flash')        FLASH=true;;
    '--help')         HELP=true;;
    '--no-flash')     FLASH=false;;
    '--no-revision')  REVISION=false;;
    '--primary')      TARGET=primary;;
    '--revision')     REVISION=true;;
    '--secondary')    TARGET=secondary;;
    "--"*)            echo "Invalid argument: $arg"; exit 2;;
  esac
done

# ==============================================================================
# LOGIC
# ==============================================================================

# ------------------------------------------------------------------------------
# Print help documentation about this build script.
# ------------------------------------------------------------------------------
if [ "${HELP}" = true ]; then
  echo "flash.sh - Build and flash Praxis firmware."
  echo ""
  echo "This utility exists to make building and flashing the Praxis firmware easier."
  echo "By default, running this script with no arguments will build and flash both"
  echo "the primary and secondary firmware images (in that order)."
  echo ""
  echo "Options:"
  echo "  --both            Target both firmware images (primary and then secondary)"
  echo "  --(no-)flash      Whether or not to flash (if false, then only build)"
  echo "  --help            Print this help message and then exit"
  echo "  --primary         Target only the primary firmware"
  echo "  --(no-)revision   Whether or not to allow the revision/hash updates"
  echo "  --secondary       Target only the secondary firmware"
  exit 0
fi

# ------------------------------------------------------------------------------
# Compute the actual hash of the files in the current directory. Omit certain
# files like build-generated ones, or resource files that aren't a part of the
# compilation process.
# ------------------------------------------------------------------------------
readonly ACTUAL_HASH=$(                                                         \
  find ${SOURCE_DIR} -type f                                                    \
    ! -path "${SOURCE_DIR}/.git/*"                                              \
    ! -path "${SOURCE_DIR}/meta/*"                                              \
    ! -path "${SOURCE_DIR}/common/meta/*"                                       \
    ! -path "${SOURCE_DIR}/resources/*"                                         \
    ! -name "flash.sh"                                                          \
    ! -name "README.md"                                                         \
    -print0                                                                     \
  | sort -z                                                                     \
  | xargs -0 sha1sum                                                            \
  | sha1sum                                                                     \
  | awk '{print $1}'                                                            \
)

# ------------------------------------------------------------------------------
# If the last hash is different from the current hash, it's a new revision.
# ------------------------------------------------------------------------------
if [ "${LAST_KNOWN_HASH}" = "${ACTUAL_HASH}" ]; then
  readonly ACTUAL_REVISION=${LAST_KNOWN_REVISION}
  echo "Praxis - rev ${ACTUAL_REVISION} - ${ACTUAL_HASH}"
else
  readonly ACTUAL_REVISION=$((${LAST_KNOWN_REVISION} + 1))
  if [ "${REVISION}" = true ]; then
    echo "${ACTUAL_REVISION}" > "${REVISION_FILE}"
    echo "${ACTUAL_HASH}" > "${HASH_FILE}"
  fi
  echo "Praxis - rev ${ACTUAL_REVISION} - ${ACTUAL_HASH} (New Revision!)"
fi

# ------------------------------------------------------------------------------
# Create the build-generated common files.
# ------------------------------------------------------------------------------
echo "#define PRAXIS_REVISION ${ACTUAL_REVISION}" > $SOURCE_DIR/common/meta/revision.h
echo "#define PRAXIS_HASH \"${ACTUAL_HASH}\"" > $SOURCE_DIR/common/meta/hash.h

# ------------------------------------------------------------------------------
# Build and flash the primary firmware (if requested).
# ------------------------------------------------------------------------------
if  [[ "${TARGET}" == "both" || "${TARGET}" == "primary" ]]; then
  echo "-----------------------------------------------------------------------"
  echo "Building primary firmware..."
  echo "#define PRAXIS_TARGET PRAXIS_PRIMARY" > ${SOURCE_DIR}/common/meta/target.h
  qmk compile -kb crkbd -km praxis
  if [ "${FLASH}" = true ]; then
    echo "Flashing primary firmware..."
    qmk flash
  fi
fi

# ------------------------------------------------------------------------------
# Build and flash the secondary firmware (if requested).
# ------------------------------------------------------------------------------
if  [[ "${TARGET}" == "both" || "${TARGET}" == "secondary" ]]; then
  echo "-----------------------------------------------------------------------"
  echo "Building secondary firmware..."
  echo "#define PRAXIS_TARGET PRAXIS_SECONDARY" > ${SOURCE_DIR}/common/meta/target.h
  qmk compile -kb crkbd -km praxis
  if [ "${FLASH}" = true ]; then
    echo "Flashing secondary firmware..."
    qmk flash
  fi
fi
