/*******************************************************************************
 * CONVERT TO QMK KEYCODES
 ******************************************************************************/

// -----------------------------------------------------------------------------
static qmk_keycode_t to_qmk_keycode_for_layer (px_hw_keycode_t keycode, uint8_t layer) {
  const uint16_t prcode = (keycode - PX_HW_KEYS_BEGIN);
  return s_keymaps_px[layer][prcode] & ~PC_COMBO;
}

// -----------------------------------------------------------------------------
static bool to_qmk_combohint_for_layer (px_hw_keycode_t keycode, uint8_t layer) {
  const uint16_t prcode = (keycode - PX_HW_KEYS_BEGIN);
  return s_keymaps_px[layer][prcode] & PC_COMBO;
}

// -----------------------------------------------------------------------------
static qmk_keycode_t to_qmk_keycode_for_default (px_keycode_t keycode) {
  uint8_t layer = s_mode & PX_MODE_LAYER_MASK;
  return to_qmk_keycode_for_layer(keycode, layer);
}

// -----------------------------------------------------------------------------
static qmk_keycode_t to_qmk_keycode_for_inner_thumb (px_keycode_t keycode) {
  // This is probably better encoded in the layout file, but this saves space.
  if ((s_mode >> PX_SUBMODE_SHIFT) == PX_SUBMODE_KILL) {
    if (s_mode & PX_MODE_RUNE_SHIFT_BIT) {
      return LALT(KC_F4);
    } else {
      return LCTL(KC_W);
    }
  }
  return to_qmk_keycode_for_default(keycode);
}

// -----------------------------------------------------------------------------
static qmk_keycode_t to_qmk_keycode (px_keycode_t keycode) {
  switch (keycode) {
    case PX_LEFT_THUMB_INNER:
    case PX_RIGHT_THUMB_INNER:
      return to_qmk_keycode_for_inner_thumb(keycode);
    default:
      return to_qmk_keycode_for_default(keycode);
  }
}

// -----------------------------------------------------------------------------
#define JOIN_KEYS(a, b) (((a) > (b)) ? ((a) | (((uint32_t)(b)) << 16)) : ((b) | (((uint32_t)(a)) << 16)))
static qmk_keycode_t to_qmk_keycode_combo (
  const px_keystate_t* ks0,
  const px_keystate_t* ks1
) {
  // Check if the key is one of the global combos that we want to handle.
  DEBUG(STR("COMBO(2):"));
  switch (JOIN_KEYS(ks0->px_keycode, ks1->px_keycode)) {
    case JOIN_KEYS(PX_LEFT_THUMB_OUTER, PX_LEFT_THUMB_HOME):
      return PX_COMBO_MODE_SWITCH_EDIT;
    case JOIN_KEYS(PX_RIGHT_THUMB_OUTER, PX_RIGHT_THUMB_HOME):
      return PX_COMBO_MODE_SWITCH_REVIEW;
    case JOIN_KEYS(PX_LEFT_THUMB_INNER, PX_RIGHT_THUMB_INNER):
      return PX_COMBO_MODE_TOGGLE;
    case JOIN_KEYS(PX_LEFT_THUMB_INNER, PX_LEFT_THUMB_OUTER):
      return PX_COMBO_DEBUG_PRIMARY_INFO;
    case JOIN_KEYS(PX_RIGHT_THUMB_INNER, PX_RIGHT_THUMB_OUTER):
      return PX_COMBO_DEBUG_SECONDARY_INFO;
    case JOIN_KEYS(PX_LEFT_THUMB_HOME, PX_LEFT_THUMB_INNER):
      return PX_COMBO_DEBUG_EVENT_LOG;
    case JOIN_KEYS(PX_RIGHT_THUMB_HOME, PX_RIGHT_THUMB_INNER):
      return PX_COMBO_DEBUG_STATS_RESET;
    case JOIN_KEYS(PX_LEFT_CONTROL, PX_3x5):
    case JOIN_KEYS(PX_RIGHT_CONTROL, PX_3x5):
      return LCTL(KC_BSPC);
    case JOIN_KEYS(PX_LEFT_CONTROL, PX_3x0):
    case JOIN_KEYS(PX_RIGHT_CONTROL, PX_3x0):
      return LCTL(KC_DEL);
    default:
      break;
  }

  // Check if the key is one of the value-specific combos to handle.
  const px_keycode_t kc0 = to_qmk_keycode(ks0->px_keycode);
  const px_keycode_t kc1 = to_qmk_keycode(ks1->px_keycode);
  switch (JOIN_KEYS(kc0, kc1)) {
    case JOIN_KEYS(PK_LCURLY, PK_RCURLY):
      return PX_COMBO_MACRO_CURLY;
    case JOIN_KEYS(PK_LPARENS, PK_RPARENS):
      return PX_COMBO_MACRO_PARENS;
    case JOIN_KEYS(PK_LSQUARE, PK_RSQUARE):
      return PX_COMBO_MACRO_SQUARE;
    case JOIN_KEYS(PK_LANGLE, PK_RANGLE):
      return PX_COMBO_MACRO_ANGLE;
    case JOIN_KEYS(PK_CARAT, PK_DOLLAR):
      return PX_COMBO_MACRO_REGEX;
    default:
      break;
  }
  #undef JOIN_PX_KEYS

  return 0;
}
#undef JOIN_KEYS
