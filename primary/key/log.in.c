/*******************************************************************************
 * PRAXIS KEY LOG
 ******************************************************************************/

// -----------------------------------------------------------------------------
static void praxis_add_event_log (
  px_hw_keycode_t     keycode,
  px_key_event_type_t event
) {
  const uint8_t keydiff = keycode - (PX_0x0 - 1);
  if (event == PX_EVENT_RELEASE) {
    if (s_event_log[0] == keydiff) {
      s_event_log[0] |= PX_EVENT_TAP;
      return;
    }
  }
  praxis_memmove(
    &s_event_log[1],
    s_event_log,
    sizeof(px_key_event_t) * (MAX_EVENT_LOG_SIZE - 1)
  );
  s_event_log[0] = event | keydiff;
}

// -----------------------------------------------------------------------------
static void praxis_clear_event_log (void) {
  praxis_memzero(s_event_log, sizeof(s_event_log));
}
