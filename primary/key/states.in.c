/*******************************************************************************
 * MANAGE KEYSTATE ARRAY
 ******************************************************************************/

// -----------------------------------------------------------------------------
static px_keystate_t* get_keystate (px_keycode_t keycode) {
  for (px_keystate_t* keystate = KEYSTATES_BEGIN; keystate != KEYSTATES_END; ++keystate) {
    if (keystate->px_keycode == keycode) {
      return keystate;
    }
  }
  return NULL;
}

// -----------------------------------------------------------------------------
static px_keycombo_t get_keycombo (px_keystate_t* keystate) {
  // NOTE: This algorithm only works because we pad the keystates with 0s.
  px_keycombo_t keycombo = {
    .begin = keystate,
    .end = &keystate[1],
  };
  if (keystate->group) {
    while (keycombo.begin[-1].group == keystate->group) {
      --keycombo.begin;
    }
    while (keycombo.end[0].group == keystate->group) {
      ++keycombo.end;
    }
  }
  return keycombo;
}

// -----------------------------------------------------------------------------
static uint8_t allocate_group (void) {
  uint8_t selection = 1;
  while (s_groups & selection) {
    selection <<= 1;
    if (selection == (1 << 7)) {
      s_last_error = PX_GROUP_ALLOCATION_LEAK;
    }
  }
  s_groups |= selection;
  return selection;
}

// -----------------------------------------------------------------------------
static void release_group (uint8_t group) {
  s_groups &= ~group;
}

// -----------------------------------------------------------------------------
static px_keystate_t* praxis_allocate_keystate_low_priority (void) {
  return &s_keystates[s_keystates_count++];
}

// -----------------------------------------------------------------------------
static px_keystate_t* praxis_allocate_keystate_high_priority (void) {
  px_keystate_t* grouped_begin = &s_keystates[s_keystates_count - 1];
  while (IS_ACTIVATED(grouped_begin) && !IS_GROUPED(grouped_begin) && !IS_SHIFT_KEY(grouped_begin->px_keycode)) {
    --grouped_begin;
  }
  ++grouped_begin;

  px_keystate_t* grouped_end = &s_keystates[s_keystates_count];
  praxis_memmove(
    &grouped_begin[1],
    grouped_begin,
    sizeof(px_keystate_t) * (grouped_end - grouped_begin)
  );
  praxis_memzero(grouped_begin, sizeof(px_keystate_t));

  ++s_keystates_count;
  return grouped_begin;
}

// -----------------------------------------------------------------------------
static px_keystate_t* praxis_allocate_keystate (px_keycode_t keycode) {
  switch (keycode) {
    case PX_LAYER_SHIFT:
    case PX_RUNE_SHIFT:
      return praxis_allocate_keystate_high_priority();
    default:
      return praxis_allocate_keystate_low_priority();
  }
}

// -----------------------------------------------------------------------------
static void praxis_keystate_add (px_keycode_t keycode, const keyrecord_t* record) {
  DEBUG(STR("ADD("), U8(s_keystates_count), STR(")"));
  // It's possible that too many keys are held, so we should swallow keys if so.
  if (s_keystates_count == MAX_KEYS_HELD) {
    s_last_error = PX_SWALLOWED_KEY;
    return; // Swallow key.
  }

  // Get the key directly before this one, if it's active and ungrouped then the
  // key should be grouped together with this new key press.
  px_keystate_t* previous = &s_keystates[s_keystates_count - 1];
  const bool should_group = IS_ACTIVATED(previous) && !IS_GROUPED(previous);
  const uint8_t group = (should_group) ? previous->group : allocate_group();

  // Initialize the keystate with new values and assign the group if needed.
  px_keystate_t* keystate = praxis_allocate_keystate(keycode);
  keystate->px_keycode = keycode;
  keystate->group = group;
  keystate->state = PX_STATE_ACTIVATED_BIT;
  keystate->pressed = record->event.time;
}

// -----------------------------------------------------------------------------
// Notice that this invalidates any existing pointers to `s_keystates`.
// -----------------------------------------------------------------------------
static void praxis_keystate_remove (px_keystate_t* keystate) {
  DEBUG(STR("REM("), U8(s_keystates_count), STR(")"));
  // If the keystate was the last from a combo press, then release the group.
  if (keystate[-1].group != keystate->group && keystate[1].group != keystate->group) {
    release_group(keystate->group);
  }

  // Erase the key and shift the other keys around, memset the new end key.
  praxis_memmove(
    keystate,
    &keystate[1],
    (&s_keystates[MAX_KEYS_HELD] - &keystate[1]) * sizeof(px_keystate_t)
  );
  s_keystates_count--;
  praxis_memzero(&s_keystates[s_keystates_count], sizeof(px_keystate_t));
}
