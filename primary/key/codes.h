/*******************************************************************************
 * PRAXIS KEYCODE MACROS
 ******************************************************************************/
#pragma once

// =============================================================================
// MODIFIER KEYS
// =============================================================================

// -----------------------------------------------------------------------------
#define PM_COMBINE_(key, combo)         ((key & combo) ? PC_INVALID : (key | combo))

// -----------------------------------------------------------------------------
#define PM________(key,combo)           PM_COMBINE_(key, combo)
#define PM_XXXXXX_(key,combo)           PM_COMBINE_(key, combo)
#define PM__TODO__(key,combo)           PM_COMBINE_(key, combo)
#define PM_SHIFT_(key,combo)            PM_COMBINE_(LSFT(key), combo)
#define PM_CTRL_(key,combo)             PM_COMBINE_(LCTL(key), combo)
#define PM_ALT_(key,combo)              PM_COMBINE_(LALT(key), combo)
#define PM_ASHIFT_(key,combo)           PM_COMBINE_(LALT(LSFT(key)), combo)
#define PM_CSHIFT_(key,combo)           PM_COMBINE_(LCTL(LSFT(key)), combo)
#define PM_HYPER_(key,combo)            PM_COMBINE_(LGUI(LALT(LCTL(key))), combo)
#define PM_SHYPER_(key,combo)           PM_COMBINE_(LGUI(LALT(LCTL(LSFT(key)))), combo)

// =============================================================================
// COMBO HINTS
// =============================================================================

// -----------------------------------------------------------------------------
#define PC_______                       0x0000
#define PC_COMBO                        0x8000
#define PC_INVALID                      0xFFFF

// =============================================================================
// KEYCODES
// =============================================================================

// -----------------------------------------------------------------------------
// Key Markers
// -----------------------------------------------------------------------------
#define PK_______                       XXXXXXX /* No Assigned Key */
#define PK_XXXXXX                       XXXXXXX /* Invalid Key Location */
#define PK__TODO_                       XXXXXXX /* Temporarily Unassigned Key */

// -----------------------------------------------------------------------------
// ASCII Keys
// -----------------------------------------------------------------------------
#define PK_A                            KC_A
#define PK_B                            KC_B
#define PK_C                            KC_C
#define PK_D                            KC_D
#define PK_E                            KC_E
#define PK_F                            KC_F
#define PK_G                            KC_G
#define PK_H                            KC_H
#define PK_I                            KC_I
#define PK_J                            KC_J
#define PK_K                            KC_K
#define PK_L                            KC_L
#define PK_M                            KC_M
#define PK_N                            KC_N
#define PK_O                            KC_O
#define PK_P                            KC_P
#define PK_Q                            KC_Q
#define PK_R                            KC_R
#define PK_S                            KC_S
#define PK_T                            KC_T
#define PK_U                            KC_U
#define PK_V                            KC_V
#define PK_W                            KC_W
#define PK_X                            KC_X
#define PK_Y                            KC_Y
#define PK_Z                            KC_Z

// -----------------------------------------------------------------------------
// Number Keys
// -----------------------------------------------------------------------------
#define PK_0                            KC_0
#define PK_1                            KC_1
#define PK_2                            KC_2
#define PK_3                            KC_3
#define PK_4                            KC_4
#define PK_5                            KC_5
#define PK_6                            KC_6
#define PK_7                            KC_7
#define PK_8                            KC_8
#define PK_9                            KC_9

// -----------------------------------------------------------------------------
// Whitespace Keys
// -----------------------------------------------------------------------------
#define PK_BACKSP                       KC_BSPC
#define PK_ENTER                        KC_ENT
#define PK_SPACE                        KC_SPC
#define PK_TAB                          KC_TAB

// -----------------------------------------------------------------------------
// Navigation Keys
// -----------------------------------------------------------------------------
#define PK_DOWN                         KC_DOWN
#define PK_LEFT                         KC_LEFT
#define PK_PGDN                         KC_PAGE_DOWN
#define PK_PGUP                         KC_PAGE_UP
#define PK_RIGHT                        KC_RIGHT
#define PK_UP                           KC_UP

// -----------------------------------------------------------------------------
// Symbol Keys
// -----------------------------------------------------------------------------
#define PK_AND                          LSFT(KC_7)
#define PK_AT                           LSFT(KC_2)
#define PK_BACKTICK                     KC_GRAVE
#define PK_CARAT                        LSFT(KC_6)
#define PK_COLON                        LSFT(KC_SCLN)
#define PK_COMMA                        KC_COMM
#define PK_DECIMAL                      KC_DOT
#define PK_DELETE                       KC_DEL
#define PK_DIVIDE                       KC_SLSH
#define PK_DOLLAR                       LSFT(KC_4)
#define PK_DQUOTE                       LSFT(KC_QUOT)
#define PK_END                          KC_END
#define PK_EQUAL                        KC_EQL
#define PK_GREATER                      LSFT(KC_DOT)
#define PK_HASH                         LSFT(KC_3)
#define PK_HOME                         KC_HOME
#define PK_LANGLE                       LSFT(KC_COMMA)
#define PK_LCURLY                       LSFT(KC_LEFT_BRACKET)
#define PK_LESS                         LSFT(KC_COMMA)
#define PK_LPARENS                      LSFT(KC_9)
#define PK_LSQUARE                      KC_LBRC
#define PK_MINUS                        KC_MINS
#define PK_MULTIPLY                     LSFT(KC_8)
#define PK_NOT                          LSFT(KC_1)
#define PK_OR                           LSFT(KC_BACKSLASH)
#define PK_PERCENT                      LSFT(KC_5)
#define PK_PERIOD                       KC_DOT
#define PK_PERIOD                       KC_DOT
#define PK_PLUS                         LSFT(KC_EQL)
#define PK_QUESTION                     LSFT(KC_SLSH)
#define PK_QUOTE                        KC_QUOT
#define PK_RANGLE                       LSFT(KC_DOT)
#define PK_RCURLY                       LSFT(KC_RIGHT_BRACKET)
#define PK_REVSLASH                     KC_BSLS
#define PK_RPARENS                      LSFT(KC_0)
#define PK_RSQUARE                      KC_RBRC
#define PK_SCOLON                       KC_SCLN
#define PK_SLASH                        KC_SLSH
#define PK_TILDE                        LSFT(KC_GRAVE)
#define PK_USCORE                       LSFT(KC_MINS)

// -----------------------------------------------------------------------------
// Functional Keys
// -----------------------------------------------------------------------------
#define PK_ESCAPE                       KC_ESC
#define PK_ESC                          KC_ESC
#define PK_F1                           KC_F1
#define PK_F2                           KC_F2
#define PK_F3                           KC_F3
#define PK_F4                           KC_F4
#define PK_F5                           KC_F5
#define PK_F6                           KC_F6
#define PK_F7                           KC_F7
#define PK_F8                           KC_F8
#define PK_F9                           KC_F9
#define PK_F10                          KC_F10
#define PK_F11                          KC_F11
#define PK_F12                          KC_F12
#define PK_F13                          KC_F13
#define PK_JPN                          KC_INT2
#define PK_PRINT                        KC_PSCR

// -----------------------------------------------------------------------------
// Media Keys
// -----------------------------------------------------------------------------
#define PK_VOL_UP                       KC_AUDIO_VOL_UP
#define PK_VOL_DN                       KC_AUDIO_VOL_DOWN
#define PK_VOL_MT                       KC_AUDIO_MUTE
#define PK_MUS_NX                       KC_MEDIA_NEXT_TRACK
#define PK_MUS_PV                       KC_MEDIA_PREV_TRACK
#define PK_MUS_PL                       KC_MEDIA_PLAY_PAUSE

// -----------------------------------------------------------------------------
// Mouse Keys
// -----------------------------------------------------------------------------
#define PK_MS_BTN1                      KC_MS_BTN1
#define PK_MS_BTN2                      KC_MS_BTN2
#define PK_MS_BTN3                      KC_MS_BTN3
#define PK_MS_BTN4                      KC_MS_BTN4
#define PK_MS_BTN5                      KC_MS_BTN5
#define PK_MS_DOWN                      KC_MS_DOWN
#define PK_MS_LEFT                      KC_MS_LEFT
#define PK_MS_PAND                      KC_MS_WH_DOWN
#define PK_MS_PANL                      KC_MS_WH_LEFT
#define PK_MS_PANR                      KC_MS_WH_RIGHT
#define PK_MS_PANU                      KC_MS_WH_UP
#define PK_MS_RIGHT                     KC_MS_RIGHT
#define PK_MS_UP                        KC_MS_UP

// -----------------------------------------------------------------------------
// Action Keys
// -----------------------------------------------------------------------------
#define PK_KILL                         PX_ACTION_KILL_MODE_TOGGLE
#define PK_PREVWIN                      PX_ACTION_PREVIOUS_WINDOW
#define PK_NEXTWIN                      PX_ACTION_NEXT_WINDOW

// =============================================================================
// MACROS
// =============================================================================

// -----------------------------------------------------------------------------
#define IS_QMK_MODIFIER_(kc) (((kc)==KC_LGUI)||((kc)==KC_RGUI)||((kc)==KC_LALT)||((kc)==KC_RALT)||((kc)==KC_LCTL)||((kc)==KC_RCTL))
#define IS_QMK_MODIFIER(ks) IS_QMK_MODIFIER_((ks)->qmk_keycode)
