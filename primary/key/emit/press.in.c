/*******************************************************************************
 * PRAXIS EMIT KEY PRESS
 ******************************************************************************/

// =============================================================================
// WPM Calculations
//   These calculations are a little more complicated than one might think. That
//   is because these calculations try to parse words like `ThisOne` into two
//   separate words. It does this using some well-worn heuristics for converting
//   UpperCamelCase symbols into other_kinds_of_symbols by word separation.
//
//   This is not an accurate thing, if you want a good WPM you should probably
//   instead be using some actual WPM calculator, but this is far better than
//   typing a ton of code with varying casing to have your WPM be 3 or something
//   else obviously incorrect.
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_abandon_current_word (bool decrease_word_count) {
  if (decrease_word_count) {
    uint32_safe_dec(&s_stats.counters.words);
    if (s_stats.word.index < PX_WPM_SAMPLES_MAX) {
      uint8_safe_dec(&s_stats.wpm.samples[s_stats.word.index]);
    }
  }
  praxis_memzero(&s_stats.word, sizeof(s_stats.word));
}

// -----------------------------------------------------------------------------
static px_char_class_t classify_character (qmk_keycode_t keycode) {
  if (keycode >= LSFT(KC_A) && keycode <= LSFT(KC_Z)) {
    return PX_CHAR_ALPHA_UPPER;
  }
  if (keycode >= KC_A && keycode <= KC_Z) {
    return PX_CHAR_ALPHA_LOWER;
  }
  if ((keycode >= KC_0 && keycode <= KC_9) || keycode == KC_QUOT) {
    return PX_CHAR_SYMBOL;
  }

  // Any other char is "undefined". These chars don't count towards word counts,
  // and will end any ongoing words.
  return PX_CHAR_UNDEFINED;
}

// -----------------------------------------------------------------------------
static void praxis_update_wpm (qmk_keycode_t keycode) {
  // Whatever we're doing, it's a modded key press, so it's ending a word.
  // Non-shift commands will always end words in this calculator.
  if (get_mods() & ~(MOD_BIT_LSHIFT | MOD_BIT_RSHIFT)) {
    praxis_abandon_current_word(false);
    return;
  }

  // If the key is a "back" key, we're probably trying to fix something. Undo
  // word length until we reach the beginning, and then give up on the word.
  if (keycode == KC_BACKSPACE || keycode == KC_LEFT) {
    if (s_stats.word.length) {
      --s_stats.word.length;
      if (!s_stats.word.length) {
        praxis_abandon_current_word(true);
      }
    }
    return;
  }

  // If it's a character past the printable range, then abandon the word. These
  // keys cannot guide our logic to make proper decisions, and we cannot be sure
  // what happened to the keyboard focus in these cases.
  if ((keycode & ~(QK_LSFT | QK_RSFT)) >= KC_CAPS_LOCK) {
    praxis_abandon_current_word(false);
    return;
  }

  // Increment the basic character counter.
  uint32_safe_inc(&s_stats.counters.chars);

  // Sample the character, and start processing it. If we cannot classify the
  // character then we should abandon the word, somehow the input was invalid.
  const px_char_class_t keyclass = classify_character(keycode);
  if (keyclass == PX_CHAR_UNDEFINED) {
    praxis_abandon_current_word(false);
    return;
  }

  // Now we need to decide when and how to "end words" within a single non-space
  // terminated string of characters. This logic is pretty tricky, and some of
  // the branches return instead of break. The logic is supposed to only break
  // if we find that we are continuing to process a word and want to increase
  // the length of the current word.
  switch (s_stats.word.format) {
    case PX_WORD_FORMAT_UNKNOWN:
      switch (keyclass) {
        case PX_CHAR_UNDEFINED:
          return;
        case PX_CHAR_ALPHA_LOWER:
          s_stats.word.format = PX_WORD_FORMAT_LOWERCASE;
          break;
        case PX_CHAR_ALPHA_UPPER:
          s_stats.word.format = PX_WORD_FORMAT_UNCERTAIN;
          break;
        case PX_CHAR_SYMBOL:
          praxis_abandon_current_word(false);
          return;
      }
      break;
    case PX_WORD_FORMAT_UNCERTAIN:
      switch (keyclass) {
        case PX_CHAR_UNDEFINED:
          return;
        case PX_CHAR_ALPHA_LOWER:
          s_stats.word.format = PX_WORD_FORMAT_TITLECASE;
          break;
        case PX_CHAR_ALPHA_UPPER:
          s_stats.word.format = PX_WORD_FORMAT_UPPERCASE;
          break;
        case PX_CHAR_SYMBOL:
          break;
      }
      break;
    case PX_WORD_FORMAT_LOWERCASE:
      switch (keyclass) {
        case PX_CHAR_UNDEFINED:
          return;
        case PX_CHAR_ALPHA_LOWER:
          break;
        case PX_CHAR_ALPHA_UPPER:
          s_stats.word.length = 0;
          s_stats.word.format = PX_WORD_FORMAT_UNCERTAIN;
          break;
        case PX_CHAR_SYMBOL:
          break;
      }
      break;
    case PX_WORD_FORMAT_UPPERCASE:
      switch (keyclass) {
        case PX_CHAR_UNDEFINED:
          return;
        case PX_CHAR_ALPHA_LOWER:
          uint8_safe_inc(&s_stats.wpm.samples[0]);
          uint32_safe_inc(&s_stats.counters.words);
          s_stats.word.length = 1;
          s_stats.word.format = PX_WORD_FORMAT_TITLECASE;
          break;
        case PX_CHAR_ALPHA_UPPER:
          break;
        case PX_CHAR_SYMBOL:
          break;
      }
      break;
    case PX_WORD_FORMAT_TITLECASE:
      switch (keyclass) {
        case PX_CHAR_UNDEFINED:
          return;
        case PX_CHAR_ALPHA_LOWER:
          break;
        case PX_CHAR_ALPHA_UPPER:
          s_stats.word.length = 0;
          s_stats.word.format = PX_WORD_FORMAT_UNCERTAIN;
          break;
        case PX_CHAR_SYMBOL:
          break;
      }
      break;
  }

  // We are about to increase the length of the current word, if we're going to
  // do this and the current length is 0, then we've started a new word. It's
  // a tad easier to start the word counter here instead of when you "terminate"
  // a word, even though it feels a slight bit inaccurate.
  if (s_stats.word.length == 0) {
    uint8_safe_inc(&s_stats.wpm.samples[0]);
    uint32_safe_inc(&s_stats.counters.words);
    s_stats.word.index = 0;
  }
  uint16_safe_inc(&s_stats.word.length);
}

// =============================================================================
// Mouse Keys
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_emit_key_press_mouse_move (
  px_mousekey_t* mousekey,
  px_keystate_t* keystate
) {
  mousekey->state = PX_STATE_ACTIVATED_BIT;
  mousekey->pressed = keystate->pressed;
}

// -----------------------------------------------------------------------------
static void praxis_emit_key_press_mouse (px_keystate_t* keystate) {
  const qmk_keycode_t keycode = keystate->qmk_keycode;
  if (IS_MOUSEKEY_BUTTON(keycode)) {
    praxis_abandon_current_word(false);
    s_mousestate.buttons |= (1 << (keycode - KC_MS_BTN1));
  } else {
    switch (keycode) {
      case KC_MS_DOWN:
        praxis_emit_key_press_mouse_move(&s_mousestate.move.down, keystate);
        break;
      case KC_MS_LEFT:
        praxis_emit_key_press_mouse_move(&s_mousestate.move.left, keystate);
        break;
      case KC_MS_RIGHT:
        praxis_emit_key_press_mouse_move(&s_mousestate.move.right, keystate);
        break;
      case KC_MS_UP:
        praxis_emit_key_press_mouse_move(&s_mousestate.move.up, keystate);
        break;
      case KC_MS_WH_DOWN:
        praxis_emit_key_press_mouse_move(&s_mousestate.wheel.down, keystate);
        s_mousestate.wheel.down.hiccup = MOUSE_WHEEL_DELAY;
        break;
      case KC_MS_WH_LEFT:
        praxis_emit_key_press_mouse_move(&s_mousestate.wheel.left, keystate);
        s_mousestate.wheel.left.hiccup = MOUSE_WHEEL_DELAY;
        break;
      case KC_MS_WH_RIGHT:
        praxis_emit_key_press_mouse_move(&s_mousestate.wheel.right, keystate);
        s_mousestate.wheel.right.hiccup = MOUSE_WHEEL_DELAY;
        break;
      case KC_MS_WH_UP:
        praxis_emit_key_press_mouse_move(&s_mousestate.wheel.up, keystate);
        s_mousestate.wheel.up.hiccup = MOUSE_WHEEL_DELAY;
        break;
    }
  }
}

// =============================================================================
// Non-Mouse Keys
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_emit_key_press_regular_mode_switch_edit (void) {
  s_mode &= ~PX_MODE_REVIEW_MODE_BIT;
}

// -----------------------------------------------------------------------------
static void praxis_emit_key_press_regular_mode_switch_review (void) {
  s_mode |= PX_MODE_REVIEW_MODE_BIT;
}

// -----------------------------------------------------------------------------
static void praxis_emit_key_press_regular_mode_switch_toggle (void) {
  s_mode ^= PX_MODE_REVIEW_MODE_BIT;
}

// -----------------------------------------------------------------------------
static void praxis_emit_key_press_regular_mode_enable (px_mode_bit_t mode) {
  s_mode |= mode;
}

// -----------------------------------------------------------------------------
static void praxis_emit_key_press_regular_submode_enable (px_submode_t mode) {
  s_mode = (s_mode & ~PX_SUBMODE_MASK) | (mode << PX_SUBMODE_SHIFT);
}

// -----------------------------------------------------------------------------
static void praxis_emit_key_press_regular_other (qmk_keycode_t keycode) {
#define MOD_BIT_GUI_MASK (MOD_BIT_LGUI|MOD_BIT_RGUI)
  praxis_update_wpm(keycode);
  DEBUG(STR("REG:"), U8(keycode));

  // If we're locking the computur, we should clear the event log.
  // The user is very likely about to leave their computer.
  const uint8_t mods = get_mods();
  if (keycode == KC_L && (mods & MOD_BIT_GUI_MASK) && !(mods & ~MOD_BIT_GUI_MASK)) {
    praxis_clear_event_log();
  }

#ifndef ENABLE_DEBUG
  register_code(keycode);
#endif
}

// -----------------------------------------------------------------------------
static void praxis_emit_key_press_regular_macro (char const* macro) {
  send_string(macro);
}

// -----------------------------------------------------------------------------
static void praxis_emit_key_press_regular_cycle_window (void) {
  // Note: The modifier keys are applied in the mods file.
  register_code(KC_TAB);
}

// -----------------------------------------------------------------------------
static void praxis_emit_key_press_regular (px_keystate_t* keystate) {
  switch (keystate->qmk_keycode) {
    case PX_LEFT_ZETTA:
      return praxis_emit_key_press_regular_mode_enable(PX_MODE_LEFT_ZETTA_BIT);
    case PX_RIGHT_ZETTA:
      return praxis_emit_key_press_regular_mode_enable(PX_MODE_RIGHT_ZETTA_BIT);
    case PX_LAYER_SHIFT:
      return praxis_emit_key_press_regular_mode_enable(PX_MODE_LAYER_SHIFT_BIT);
    case PX_RUNE_SHIFT:
      return praxis_emit_key_press_regular_mode_enable(PX_MODE_RUNE_SHIFT_BIT);
    case PX_ACTION_KILL_MODE_TOGGLE:
      return praxis_emit_key_press_regular_submode_enable(PX_SUBMODE_KILL);
    case PX_ACTION_PREVIOUS_WINDOW:
      return praxis_emit_key_press_regular_cycle_window();
    case PX_ACTION_NEXT_WINDOW:
      return praxis_emit_key_press_regular_cycle_window();
    case PX_COMBO_DEBUG_PRIMARY_INFO:
      return praxis_emit_key_press_regular_submode_enable(PX_SUBMODE_PRIMARY_INFO);
    case PX_COMBO_DEBUG_SECONDARY_INFO:
      return praxis_emit_key_press_regular_mode_enable(PX_MODE_SHOW_SECONDARY_INFO_BIT);
    case PX_COMBO_DEBUG_EVENT_LOG:
      return praxis_emit_key_press_regular_submode_enable(PX_SUBMODE_DEBUG);
    case PX_COMBO_MODE_SWITCH_EDIT:
      return praxis_emit_key_press_regular_mode_switch_edit();
    case PX_COMBO_MODE_SWITCH_REVIEW:
      return praxis_emit_key_press_regular_mode_switch_review();
    case PX_COMBO_MODE_TOGGLE:
      return praxis_emit_key_press_regular_mode_switch_toggle();
    case PX_COMBO_MACRO_CURLY:
      return praxis_emit_key_press_regular_macro("{}");
    case PX_COMBO_MACRO_PARENS:
      return praxis_emit_key_press_regular_macro("()");
    case PX_COMBO_MACRO_SQUARE:
      return praxis_emit_key_press_regular_macro("[]");
    case PX_COMBO_MACRO_ANGLE:
      return praxis_emit_key_press_regular_macro("<>");
    case PX_COMBO_MACRO_REGEX:
      return praxis_emit_key_press_regular_macro("^$");
    default:
      return praxis_emit_key_press_regular_other(keystate->qmk_keycode);
  }
}

// =============================================================================
// Main Function
// =============================================================================

// -----------------------------------------------------------------------------
static bool praxis_emit_key_press (px_keystate_t* keystate) {
  if (!IS_MOUSEKEY(keystate->qmk_keycode)) {
    if (praxis_update_modifier_keys()) {
      return false;
    }
  }
  if (IS_MOUSEKEY(keystate->qmk_keycode)) {
    praxis_emit_key_press_mouse(keystate);
  } else {
    praxis_emit_key_press_regular(keystate);
  }
  return true;
}
