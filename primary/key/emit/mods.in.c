/*******************************************************************************
 * TOGGLE MOD STATES
 ******************************************************************************/

// =============================================================================
// About
//   Modifier are implemented in a way that may at first seem counter to what
//   one might expect. There is a feature in Praxis that says shift should be
//   handled in a bit of a special way; if you hold down RUNE shift, that is
//   roughly equivalent to holding down shift on a normal keyboard. However, we
//   allow you to override the shift key for single key events to allow moving
//   them to the shifted layer if desired.
//
//   One example of how this is used is the period key (.), this key is an
//   unshifted key, and yet we place it on a shifted layer (the shift of comma).
//   All keys have encoded within them what keys they should be pressing for the
//   given key action. So if shift or control must be held or released, that is
//   known by the keycode.
//
//   What this logic does is looks at the sum of all pressed keys, and then it
//   calculates which mods are expected to be pressed, then it looks at what is
//   currently pressed fixes up the differences.
//
//   Whether or not the key is shifted depends on the latest key being pressed.
//   It can be one of three options:
//
//   *   SHIFTED - The key must have shift held down.
//   *   TRANSPARENT - The key must not change the current shifted state.
//   *   UNSHIFTED - The key must have shift released.
//
//   Other modifier keys (alt, control, meta) are much simpler. If they are held
//   down (their key is held within the list of active keys), then the modifier
//   must remain held.
// =============================================================================

// =============================================================================
// Helper Functions
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_emit_single_mod_press (uint8_t mod) {
  register_mods(mod);
}

// -----------------------------------------------------------------------------
static void praxis_emit_single_mod_release (uint8_t mod) {
  unregister_mods(mod);
}

// -----------------------------------------------------------------------------
static uint8_t praxis_get_target_mods_for_keystate_mod (
  uint8_t mods,
  uint8_t left,
  uint8_t right,
  uint8_t left_bit,
  uint8_t right_bit
) {
  const uint8_t mod = mods & (left | right);
  if (mod == left) {
    return left_bit;
  }
  if (mod == right) {
    return right_bit;
  }
  return 0;
}

// -----------------------------------------------------------------------------
static uint8_t praxis_get_target_mods_for_keystate_base (
  const px_keystate_t* keystate
) {
  switch (keystate->qmk_keycode) {
    case KC_LALT:
      return MOD_BIT_LALT;
    case KC_LCTL:
      return MOD_BIT_LCTRL;
    case KC_LGUI:
      return MOD_BIT_LGUI;
    case KC_LSFT:
      return MOD_BIT_LSHIFT;
    case KC_RALT:
      return MOD_BIT_RALT;
    case KC_RCTL:
      return MOD_BIT_RCTRL;
    case KC_RGUI:
      return MOD_BIT_RGUI;
    case KC_RSFT:
      return MOD_BIT_RSHIFT;
    default:
      return 0;
  }
}

// -----------------------------------------------------------------------------
static uint8_t praxis_get_target_mods_for_keystate (
  px_keystate_t const* keystate
) {
  const uint8_t mods = QK_MODS_GET_MODS(keystate->qmk_keycode);

  return (
    praxis_get_target_mods_for_keystate_mod(mods, MOD_LALT, MOD_RALT, MOD_BIT_LALT, MOD_BIT_RALT) |
    praxis_get_target_mods_for_keystate_mod(mods, MOD_LCTL, MOD_RCTL, MOD_BIT_LCTRL, MOD_BIT_RCTRL) |
    praxis_get_target_mods_for_keystate_mod(mods, MOD_LGUI, MOD_RGUI, MOD_BIT_LGUI, MOD_BIT_RGUI) |
    praxis_get_target_mods_for_keystate_mod(mods, MOD_LSFT, MOD_RSFT, MOD_BIT_LSHIFT, MOD_BIT_RSHIFT) |
    praxis_get_target_mods_for_keystate_base(keystate)
  );
}

// -----------------------------------------------------------------------------
static uint8_t praxis_get_target_mods (void) {
  uint8_t final = 0;
  for (px_keystate_t const* keystate = KEYSTATES_BEGIN; keystate != KEYSTATES_END; ++keystate) {
    if (!IS_PRESSED(keystate) || IS_RELEASED(keystate)) {
      continue;
    }

    // The shift keys actually do hold shift down until we see we don't need it.
    if (keystate->qmk_keycode == PX_RUNE_SHIFT) {
      final |= MOD_BIT_RSHIFT;
      continue;
    }
    if (keystate->qmk_keycode == PX_LAYER_SHIFT) {
      final |= MOD_BIT_LSHIFT;
      continue;
    }

    // Special Feature: Function keys can be shifted with the zetta key.
    if ((s_mode & PX_MODE_ZETTA_MASK) && IS_FUNCTION_KEY(keystate->qmk_keycode)) {
      final |= MOD_BIT_LSHIFT;
      continue;
    }

    // Some of the PX_* keys have special handling. Most of them want shift to
    // be off by default, but then others may want additional modifiers that we
    // can't mix into the QMK keycode value. Handle those specially here.
    if (IS_QK_USER(keystate->qmk_keycode)) {
      final &= ~MOD_MASK_SHIFT;
      switch (keystate->qmk_keycode) {
        case PX_ACTION_NEXT_WINDOW:
          final |= MOD_BIT_LALT;
          break;
        case PX_ACTION_PREVIOUS_WINDOW:
          final |= MOD_BIT_LSHIFT|MOD_BIT_LALT;
          break;
        default:
          break;
      }
      continue;
    }

    // Compute the target from the keycode by mixing in the shift expectations.
    // Both shift keys are consideres equivalent from the perspective of this
    // calculator. So as long as any shift key is held down, then it will
    // satisfy the constraint of a target modifier requiring shift held.
    const uint8_t target = praxis_get_target_mods_for_keystate(keystate);
    if (IS_QMK_MODIFIER(keystate)) {
      final = (final | target);
    } else if (((final & MOD_MASK_SHIFT) != 0) != ((target & MOD_MASK_SHIFT) != 0)) {
      final = (target & MOD_MASK_SHIFT) | ((final | target) & ~MOD_MASK_SHIFT);
    } else {
      final = (final & MOD_MASK_SHIFT) | ((final | target) & ~MOD_MASK_SHIFT);
    }
  }
  return final;
}

// -----------------------------------------------------------------------------
static void praxis_update_modifier_key (
  uint8_t current,
  uint8_t target,
  uint8_t bit
) {
  const bool is_active = (current & bit);
  const bool should_be_active = (target & bit);
  if (!is_active && should_be_active) {
    praxis_emit_single_mod_press(bit);
  } else if (is_active && !should_be_active) {
    praxis_emit_single_mod_release(bit);
  }
}

// =============================================================================
// Main Function
// =============================================================================

// -----------------------------------------------------------------------------
static bool praxis_update_modifier_keys (void) {
  const uint8_t current = get_mods();
  const uint8_t target = praxis_get_target_mods();
  bool changed = false;
  if (current != target) {
    DEBUG(STR("MOD:"), U8(target));
    changed = true;
  }

  // You might be tempted to use `set_mods`, but don't do it - it's a trap.
  // My experience has been that it doesn't handle releasing certain mod keys
  // very well. So I've implemented the mod press/release custom here.
  praxis_update_modifier_key(current, target, MOD_BIT_LALT);
  praxis_update_modifier_key(current, target, MOD_BIT_LCTRL);
  praxis_update_modifier_key(current, target, MOD_BIT_LGUI);
  praxis_update_modifier_key(current, target, MOD_BIT_LSHIFT);
  praxis_update_modifier_key(current, target, MOD_BIT_RALT);
  praxis_update_modifier_key(current, target, MOD_BIT_RCTRL);
  praxis_update_modifier_key(current, target, MOD_BIT_RGUI);
  praxis_update_modifier_key(current, target, MOD_BIT_RSHIFT);

  return changed;
}
