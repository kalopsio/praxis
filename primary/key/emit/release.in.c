/*******************************************************************************
 * PRAXIS EMIT KEY RELEASE
 ******************************************************************************/

// =============================================================================
// Mouse Keys
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_emit_key_release_mouse_move (
  px_mousekey_t* mousekey,
  px_keystate_t* keystate
) {
  mousekey->state |= PX_STATE_DEACTIVATED_BIT;
  mousekey->released = keystate->released;
}

// -----------------------------------------------------------------------------
static void praxis_emit_key_release_mouse (px_keystate_t* keystate) {
  const qmk_keycode_t keycode = keystate->qmk_keycode;
  if (IS_MOUSEKEY_BUTTON(keycode)) {
    s_mousestate.buttons &= ~(1 << (keycode - KC_MS_BTN1));
  } else {
    switch (keycode) {
      case KC_MS_DOWN:
        praxis_emit_key_release_mouse_move(&s_mousestate.move.down, keystate);
        break;
      case KC_MS_LEFT:
        praxis_emit_key_release_mouse_move(&s_mousestate.move.left, keystate);
        break;
      case KC_MS_RIGHT:
        praxis_emit_key_release_mouse_move(&s_mousestate.move.right, keystate);
        break;
      case KC_MS_UP:
        praxis_emit_key_release_mouse_move(&s_mousestate.move.up, keystate);
        break;
      case KC_MS_WH_DOWN:
        praxis_emit_key_release_mouse_move(&s_mousestate.wheel.down, keystate);
        break;
      case KC_MS_WH_LEFT:
        praxis_emit_key_release_mouse_move(&s_mousestate.wheel.left, keystate);
        break;
      case KC_MS_WH_RIGHT:
        praxis_emit_key_release_mouse_move(&s_mousestate.wheel.right, keystate);
        break;
      case KC_MS_WH_UP:
        praxis_emit_key_release_mouse_move(&s_mousestate.wheel.up, keystate);
        break;
    }
  }
}

// =============================================================================
// Non-Mouse Keys
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_emit_key_release_regular_mode_enable (px_mode_bit_t mode) {
  s_mode &= ~mode;
}
// -----------------------------------------------------------------------------
static void praxis_emit_key_release_regular_submode_enable (void) {
  s_mode &= ~PX_SUBMODE_MASK;
}

// -----------------------------------------------------------------------------
static void praxis_emit_key_release_regular_other (qmk_keycode_t keycode) {
  if (IS_QK_USER(keycode)) {
    return;
  }
  DEBUG(STR("UNR:"), U8(keycode));
#ifndef ENABLE_DEBUG
  unregister_code(keycode);
#endif
}

// -----------------------------------------------------------------------------
static void praxis_emit_key_release_regular_window_cycle (void) {
  unregister_code(KC_TAB);
}
// -----------------------------------------------------------------------------
static void praxis_emit_key_release_regular_stats_reset (void) {
  praxis_memzero(&s_stats, sizeof(s_stats));
  praxis_clear_event_log();
  s_last_error = PX_ERROR_NONE;
}

// -----------------------------------------------------------------------------
static void praxis_emit_key_release_regular (px_keystate_t* keystate) {
  switch (keystate->qmk_keycode) {
    case PX_LEFT_ZETTA:
      return praxis_emit_key_release_regular_mode_enable(PX_MODE_LEFT_ZETTA_BIT);
    case PX_RIGHT_ZETTA:
      return praxis_emit_key_release_regular_mode_enable(PX_MODE_RIGHT_ZETTA_BIT);
    case PX_LAYER_SHIFT:
      return praxis_emit_key_release_regular_mode_enable(PX_MODE_LAYER_SHIFT_BIT);
    case PX_RUNE_SHIFT:
      return praxis_emit_key_release_regular_mode_enable(PX_MODE_RUNE_SHIFT_BIT);
    case PX_ACTION_KILL_MODE_TOGGLE:
      return praxis_emit_key_release_regular_submode_enable();
    case PX_ACTION_PREVIOUS_WINDOW:
      return praxis_emit_key_release_regular_window_cycle();
    case PX_ACTION_NEXT_WINDOW:
      return praxis_emit_key_release_regular_window_cycle();
    case PX_COMBO_DEBUG_PRIMARY_INFO:
      return praxis_emit_key_release_regular_submode_enable();
    case PX_COMBO_DEBUG_SECONDARY_INFO:
      return praxis_emit_key_release_regular_mode_enable(PX_MODE_SHOW_SECONDARY_INFO_BIT);
    case PX_COMBO_DEBUG_EVENT_LOG:
      return praxis_emit_key_release_regular_submode_enable();
    case PX_COMBO_DEBUG_STATS_RESET:
      return praxis_emit_key_release_regular_stats_reset();
    default:
      return praxis_emit_key_release_regular_other(keystate->qmk_keycode);
  }
}

// =============================================================================
// Main Function
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_emit_key_release (px_keystate_t* keystate) {
  if (IS_MOUSEKEY(keystate->qmk_keycode)) {
    praxis_emit_key_release_mouse(keystate);
  } else {
    praxis_emit_key_release_regular(keystate);
  }

  // Special Feature: The prev/next window action won't immediately release the
  // required held modifiers. This is useful because some window managers like
  // to show a little window selector as long as tab is held. This delays the
  // release of tab until another key is pressed, or the shift key is released.
  const bool is_window_cycle = (
    keystate->qmk_keycode == PX_ACTION_NEXT_WINDOW ||
    keystate->qmk_keycode == PX_ACTION_PREVIOUS_WINDOW
  );
  if (!IS_MOUSEKEY(keystate->qmk_keycode) && !is_window_cycle) {
    praxis_update_modifier_keys();
  }
}
