/*******************************************************************************
 * PRAXIS MATH LIBRARY (BASE)
 ******************************************************************************/

// -----------------------------------------------------------------------------
static void float_saturate (float* f) {
  if (*f > 1.0f) {
    *f = 1.0f;
  }
}

// -----------------------------------------------------------------------------
static int8_t int32_clamp8 (int32_t value, int8_t min, int8_t max) {
  if (value > max) {
    return max;
  }
  if (value < min) {
    return min;
  }
  return (int8_t)value;
}

// -----------------------------------------------------------------------------
static void uint8_safe_inc (uint8_t *value) {
  if (*value < UINT8_MAX) {
    ++*value;
  }
}

// -----------------------------------------------------------------------------
static void uint8_safe_dec (uint8_t *value) {
  if (*value > 0) {
    --*value;
  }
}

// -----------------------------------------------------------------------------
static void uint16_safe_inc (uint16_t *value) {
  if (*value < UINT16_MAX) {
    ++*value;
  }
}

// -----------------------------------------------------------------------------
static void uint32_safe_inc (uint32_t *value) {
  if (*value < UINT32_MAX) {
    ++*value;
  }
}

// -----------------------------------------------------------------------------
static void uint32_safe_dec (uint32_t *value) {
  if (*value > 0) {
    --*value;
  }
}
