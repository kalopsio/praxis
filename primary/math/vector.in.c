/*******************************************************************************
 * PRAXIS MATH LIBRARY (VECTORS)
 ******************************************************************************/

// -----------------------------------------------------------------------------
typedef struct vector_t {
  float x;
  float y;
} vector_t;

// -----------------------------------------------------------------------------
static float vector_magnitude (vector_t* vector) {
  return sqrtf((vector->x * vector->x) + (vector->y * vector->y));
}

// -----------------------------------------------------------------------------
static void vector_normalize (vector_t* vector) {
  const float magnitude = vector_magnitude(vector);
  if (magnitude != 0.0f) {
    vector->x /= magnitude;
    vector->y /= magnitude;
  }
}
