/*******************************************************************************
 * PRAXIS EVENT HANDLERS (INITIALIZATION)
 ******************************************************************************/

// =============================================================================
// Main Handler
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_handle_init (void) {
  s_stats.wpm.timer = timer_read();
#ifdef    RGBLIGHT_ENABLE
#ifdef    ENABLE_RGB
  rgblight_enable_noeeprom();
  rgblight_sethsv_noeeprom(180, 255, 255);
  rgblight_mode_noeeprom(RGBLIGHT_MODE_BREATHING);
#else  // ENABLE_RGB
  rgblight_disable_noeeprom();
#endif // ENABLE_RGB
#endif // RGBLIGHT_ENABLE
}
