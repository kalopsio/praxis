/*******************************************************************************
 * PRAXIS EVENT HANDLERS (UPDATE)
 ******************************************************************************/

// =============================================================================
// Group Update
// =============================================================================

// -----------------------------------------------------------------------------
static bool praxis_pending_key_may_group (const px_keystate_t* keystate) {
  const px_mode_t layer = s_mode & PX_MODE_LAYER_MASK;
  return to_qmk_combohint_for_layer(keystate->px_keycode, layer);
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_group (px_keystate_t* keystate) {
  px_keycombo_t keycombo = get_keycombo(keystate);
  for (px_keystate_t* key = keycombo.begin; key != keycombo.end; ++key) {
    DEBUG(STR("G:"), U8(key->group));
    key->state |= PX_STATE_GROUPED_BIT;
  }
}

// =============================================================================
// Pressing Update
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_handle_update_pressing_single_normal (
  px_keystate_t* keystate
) {
  keystate->qmk_keycode = to_qmk_keycode(keystate->px_keycode);
  keystate->state |= PX_STATE_PRESSING_BIT;
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_pressing_single_tap_common (
  px_keystate_t* keystate,
  qmk_keycode_t keycode,
  bool shorted
) {
  // We want to sample the QMK keycode as early as possible.
  // Capture it and store it on the keystate right away, but don't activate yet.
  // If the layer key should be active instead we will write over this value.
  if (!keystate->qmk_keycode) {
    keystate->qmk_keycode = to_qmk_keycode(keystate->px_keycode);
  }

  // If the key deactivates, then we pressed it, otherwise see if it was a hold.
  if (IS_DEACTIVATED(keystate)) {
    keystate->state |= PX_STATE_PRESSING_BIT;
  } else if (shorted || timer_elapsed(keystate->pressed) >= HOLDING_TERM) {
    keystate->qmk_keycode = keycode;
    keystate->state |= PX_STATE_PRESSING_BIT;
  }
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_pressing_single_modtap (
  px_keystate_t* keystate,
  qmk_keycode_t modkey
) {
  // There are a few circumstances where we short the mod press term. If the
  // next key is already deactivated, or if the next key is a mousekey.
  const px_keystate_t* right = keystate + 1;
  const bool shorted = (
    IS_DEACTIVATED(right) ||
    IS_MOUSEKEY(to_qmk_keycode(right->px_keycode))
  );
  praxis_handle_update_pressing_single_tap_common(keystate, modkey, shorted);
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_pressing_single_layertap (
  px_keystate_t* keystate,
  qmk_keycode_t layerkey
) {
  // If you press another key after pressing a layer, then the assumption is you
  // want the layer key to activate. So we should check for other keys after us.
  if (&keystate[1] != KEYSTATES_END) {
    keystate->qmk_keycode = layerkey;
    keystate->state |= PX_STATE_PRESSING_BIT;
    return;
  }
  praxis_handle_update_pressing_single_tap_common(keystate, layerkey, false);
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_pressing_single (px_keystate_t* keystate) {
  switch (keystate->px_keycode) {
    case PX_LEFT_ZETTA:
      return praxis_handle_update_pressing_single_modtap(keystate, PX_LEFT_ZETTA);
    case PX_RIGHT_ZETTA:
      return praxis_handle_update_pressing_single_modtap(keystate, PX_RIGHT_ZETTA);
    case PX_LEFT_GUI:
      return praxis_handle_update_pressing_single_modtap(keystate, KC_LGUI);
    case PX_LEFT_ALT:
      return praxis_handle_update_pressing_single_modtap(keystate, KC_LALT);
    case PX_LEFT_CONTROL:
      return praxis_handle_update_pressing_single_modtap(keystate, KC_LCTL);
    case PX_RIGHT_CONTROL:
      return praxis_handle_update_pressing_single_modtap(keystate, KC_RCTL);
    case PX_RIGHT_ALT:
      return praxis_handle_update_pressing_single_modtap(keystate, KC_RALT);
    case PX_RIGHT_GUI:
      return praxis_handle_update_pressing_single_modtap(keystate, KC_RGUI);
    case PX_LAYER_SHIFT:
      return praxis_handle_update_pressing_single_layertap(keystate, PX_LAYER_SHIFT);
    case PX_RUNE_SHIFT:
      return praxis_handle_update_pressing_single_layertap(keystate, PX_RUNE_SHIFT);
    default:
      return praxis_handle_update_pressing_single_normal(keystate);
  }
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_pressing_group (px_keystate_t* keystate) {
  px_keycombo_t keycombo = get_keycombo(keystate);

  // Iterate over the possible valid permutations of the combo, and if any of
  // them resolve then we should consider the combo "pressed". We accept the
  // first combo that resolves, and if none do we bail.
  for (px_keystate_t* second = &keycombo.begin[1]; second != keycombo.end; ++second) {
    const qmk_keycode_t keycode = to_qmk_keycode_combo(keycombo.begin, second);
    if (!keycode) {
      continue;
    }

    // Shuffle the contents of the key array around so that the combo keys are
    // contiguous with one-another (that's how we address combos in this code).
    const px_keystate_t temp = *second;
    praxis_memmove(
      &keycombo.begin[2],
      &keycombo.begin[1],
      sizeof(*keystate) * (second - &keycombo.begin[1])
    );
    keycombo.begin[1] = temp;

    // If the combo was "split", we need to allocate a new group. This happens
    // when there are more than 2 keys and we've detected a combo. It's always
    // this because all our combos are only 2 keys long.
    uint8_t group = keystate->group;
    if ((keycombo.end - keycombo.begin) > 2) {
      group = allocate_group();
    }

    // The permutation of the combo does resolve, so we should "press" it.
    for (px_keystate_t* key = keycombo.begin; key != &keycombo.begin[2]; ++key) {
      key->qmk_keycode = keycode;
      key->state |= PX_STATE_PRESSING_BIT;
      key->group = group;
    }

    // Now return so that the rest of this key press can be processed.
    // We will handle any remaining part of this combo in a later pass.
    return;
  }

  // The combo did not resolve. We should separate the first key from the combo
  // and press it, then in a later iteration we may find that the remaining keys
  // are part of a separate combo.
  if (keycombo.end - keycombo.begin == 1) {
    release_group(keystate->group);
  }
  keystate->group = 0;
  praxis_handle_update_pressing_single(keystate);
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_pressing (px_keystate_t* keystate) {
  if (keystate->group) {
    praxis_handle_update_pressing_group(keystate);
  } else {
    praxis_handle_update_pressing_single(keystate);
  }
}

// =============================================================================
// Pressed Update
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_handle_update_pressed_group (px_keystate_t* keystate) {
  px_keycombo_t keycombo = get_keycombo(keystate);
  for (px_keystate_t* key = keycombo.begin; key != keycombo.end; ++key) {
    key->state |= PX_STATE_PRESSED_BIT;
  }

  // If processing the key press fails, undo the pressed bit so we try again.
  // Processing the key press can fail if we try to change mods too quickly.
  if (!praxis_emit_key_press(keystate)) {
    for (px_keystate_t* key = keycombo.begin; key != keycombo.end; ++key) {
      key->state &= ~PX_STATE_PRESSED_BIT;
    }
  }
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_pressed_single (px_keystate_t* keystate) {
  keystate->state |= PX_STATE_PRESSED_BIT;

  // If processing the key press fails, undo the pressed bit so we try again.
  // Processing the key press can fail if we try to change mods too quickly.
  if (!praxis_emit_key_press(keystate)) {
    keystate->state &= ~PX_STATE_PRESSED_BIT;
  }
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_pressed (px_keystate_t* keystate) {
  if (keystate->group) {
    praxis_handle_update_pressed_group(keystate);
  } else {
    praxis_handle_update_pressed_single(keystate);
  }
}

// =============================================================================
// Releasing Update
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_handle_update_releasing_shift (px_keystate_t* keystate) {
  if (timer_elapsed(keystate->released) < SHIFT_RELEASE_TERM) {
    return;
  }
  keystate->state |= PX_STATE_RELEASING_BIT;
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_releasing_normal (px_keystate_t* keystate) {
  keystate->state |= PX_STATE_RELEASING_BIT;
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_releasing_mousekey (px_keystate_t* keystate) {
  // Mouse keys are strange, and some double-click actions can be swallowed if
  // you don't have at least a one-tick delay in there. No clue why.
  if (keystate->count < 1) {
    ++keystate->count;
    return;
  }
  keystate->state |= PX_STATE_RELEASING_BIT;
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_releasing_other (px_keystate_t* keystate) {
  if (IS_MOUSEKEY_BUTTON(keystate->qmk_keycode)) {
    return praxis_handle_update_releasing_mousekey(keystate);
  }
  return praxis_handle_update_releasing_normal(keystate);
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_releasing (px_keystate_t* keystate) {
  switch (keystate->px_keycode) {
    case PX_LAYER_SHIFT:
    case PX_RUNE_SHIFT:
      return praxis_handle_update_releasing_shift(keystate);
    default:
      return praxis_handle_update_releasing_other(keystate);
  }
}

// =============================================================================
// Released Update
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_handle_update_released_group (px_keystate_t* keystate) {
  px_keycombo_t keycombo = get_keycombo(keystate);
  keystate->state |= PX_STATE_RELEASED_BIT;
  for (px_keystate_t* key = keycombo.begin; key != keycombo.end; ++key) {
    if (!(key->state & PX_STATE_RELEASED_BIT)) {
      return;
    }
  }
  praxis_emit_key_release(keystate);
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_released_single (px_keystate_t* keystate) {
  keystate->state |= PX_STATE_RELEASED_BIT;
  praxis_emit_key_release(keystate);
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_released (px_keystate_t* keystate) {
  if (keystate->group) {
    praxis_handle_update_released_group(keystate);
  } else {
    praxis_handle_update_released_single(keystate);
  }
}

// =============================================================================
// Finished Update
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_handle_update_finished (px_keystate_t* keystate) {
  const bool is_modkey = IS_MODIFIER_KEY(keystate->px_keycode);
  const bool delayed_release = is_modkey && !IS_QMK_MODIFIER(keystate);
  if (delayed_release && timer_elapsed(keystate->released) < DOUBLE_TAP_TERM) {
    return;
  }
  keystate->state |= PX_STATE_FINISHED_BIT;
}

// =============================================================================
// Key Update
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_handle_update_keys (void) {
  // We will always want to group keys first thing. That's because while we
  // process keys in various states, we may early-out, and we don't want that
  // to short-circut the grouping mechanism.
  for (px_keystate_t* key = KEYSTATES_BEGIN; key != KEYSTATES_END; ++key) {
    if (IS_GROUPED(key)) {
      continue;
    }

    // Update the pending variable, so that we wait on all potential groups.
    const bool may_group = praxis_pending_key_may_group(key);

    // If the timer is not elapsed then we need to wait longer. In this case,
    // the first one of these we run into is good enough to abort this loop.
    // This is because the keys are sorted in order of when they were pressed.
    if (may_group && timer_elapsed(key->pressed) < COMBO_GROUPING_TERM && !IS_DEACTIVATED(key)) {
      continue;
    }

    // Otherwise, group the keystates together and then continue to the next.
    praxis_handle_update_group(key);
  }

  // Transition into the pressing state is also special. If any key claims to
  // be uncertain of this transition, then all remaining keys must also wait.
  // This is because the keys need to press in the same logical order.
  for (px_keystate_t* key = KEYSTATES_BEGIN; key != KEYSTATES_END; ++key) {
    if (!IS_GROUPED(key) || IS_PRESSED(key)) {
      continue;
    }

    // If the key is not approved for pressing, check and see if it is now.
    // If it's not still, then it's probably being processed as a hold.
    // When there is a hold, we cannot process the remaining keys, so break.
    if (!IS_PRESSING(key)) {
      praxis_handle_update_pressing(key);
      if (!IS_PRESSING(key)) {
        break;
      }
    }

    // If this key has entered the pressing phase, then we should also update it
    // to be pressed. We need to do this while handling the pressing transitions
    // because sometimes when a key becomes pressed, it changes what other keys
    // do while they are pressing.
    praxis_handle_update_pressed(key);
    if (!IS_PRESSED(key)) {
      break;
    }
  }

  // Then process all of the keys that are past the pressing state.
  // From this point on the only viable state transitions are:
  //
  //   PRESSING -> PRESSED -> RELEASING -> RELEASED
  //                  ^-----------/
  //
  // This is because a key that is pending release may be re-pressed and put
  // immediately back into the pressed state. This only impacts keys with a
  // delayed release, like the rune and layer shift keys.
  for (px_keystate_t* key = KEYSTATES_BEGIN; key != KEYSTATES_END; ++key) {
    if (!IS_PRESSED(key)) {
      continue;
    }

    // Deactivated state is only set by physically releasing the key.
    if (!IS_DEACTIVATED(key)) {
      continue;
    }

    if (!IS_RELEASING(key)) {
      praxis_handle_update_releasing(key);
      if (!IS_RELEASING(key)) {
        continue;
      }
    }

    if (!IS_RELEASED(key)) {
      praxis_handle_update_released(key);
    }

    if (!IS_FINISHED(key)) {
      praxis_handle_update_finished(key);
    }
  }

  // Finally remove all released keys, we don't need to track them any more.
  for (px_keystate_t* key = KEYSTATES_BEGIN; key != KEYSTATES_END;) {
    if (IS_FINISHED(key)) {
      praxis_keystate_remove(key);
    } else {
      ++key;
    }
  }
}

// =============================================================================
// Secondary Update
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_handle_update_wpm (void) {
  // Update the second-counting timer. This number should increase and wrap.
  if (timer_elapsed(s_stats.wpm.timer) > 1000) {
    s_stats.wpm.timer = timer_read();
    ++s_stats.wpm.index; // Overflow OK
    uint8_safe_inc(&s_stats.word.index);
    praxis_memmove(
      &s_stats.wpm.samples[1],
      &s_stats.wpm.samples[0],
      60 * sizeof(s_stats.wpm.samples[0])
    );
    s_stats.wpm.samples[0] = 0;
  }
}

// -----------------------------------------------------------------------------
static bool praxis_update_display_equal (
  const px_update_display_t* lhs,
  const px_update_display_t* rhs
) {
  return praxis_memcmp(lhs, rhs, sizeof(px_update_display_t)) == 0;
}

// -----------------------------------------------------------------------------
static void praxis_handle_update_secondary (void) {
  static qmk_timestamp_t retry_timer = 0;
  static px_update_display_t last_update = {};

  // Calculate what the update would have looked like.
  px_update_display_t update = {
    .mode = s_mode,
    .oled_state = s_oled_state,
    .brightness = oled_get_brightness(),
    .counter = s_stats.wpm.index,
    .wpm = s_stats.wpm.samples[0],
    .max = s_stats.wpm.max,
    .chars = s_stats.counters.chars,
    .words = s_stats.counters.words,
    .keys = s_stats.counters.keys,
  };
  for (uint8_t idx = 1; idx < 60; ++idx) {
    if (UINT16_MAX - update.wpm >= s_stats.wpm.samples[idx]) {
      update.wpm += s_stats.wpm.samples[idx];
    } else {
      update.wpm = UINT16_MAX;
    }
  }
  if (update.wpm > s_stats.wpm.max) {
    s_stats.wpm.max = update.wpm;
    update.max = update.wpm;
  }

  // We only want to send an update if the data has changed, but to be safe we
  // will also send a redundant update even if nothing has changed since last
  // time. Most of the time the first update should go through though.
  const bool update_needed = (
    !praxis_update_display_equal(&update, &last_update) ||
    timer_elapsed(retry_timer) >= OLED_SECONDARY_UPDATE_TERM
  );
  if (!update_needed) {
    return;
  }

  transaction_rpc_send(SECONDARY_DISPLAY_HANDLER, sizeof(update), &update);
  retry_timer = timer_read();
  last_update = update;
}

// =============================================================================
// Main Handler
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_handle_update (void) {
  praxis_handle_update_keys();
  praxis_handle_update_wpm();
  praxis_handle_update_secondary();
  debug_finalize();
}
