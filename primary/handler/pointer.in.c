/*******************************************************************************
 * PRAXIS EVENT HANDLERS (POINTING DEVICE)
 ******************************************************************************/

// =============================================================================
// Jerk Processing
//   Jerks are a constant movement at the start of the button press. They arise
//   due to there sometimes being a delay between pressing the button and it
//   being processed as movement.
//   Praxis tries to minimize the amount of jerkiness there is by minimizing
//   this delay, but we still want to properly account for jerks in case some
//   future iteration of the layout introduces a larger delay.
// =============================================================================
#ifdef ENABLE_POINTER_JERKS

// -----------------------------------------------------------------------------
static float praxis_pointer_get_jerk_scalar (px_mousekey_t* mousekey) {
  if (!IS_ACTIVATED(mousekey) || IS_GROUPED(mousekey)) {
    return 0.0f;
  }

  // Mark the keystate as grouped to say its jerks have been processed.
  mousekey->state |= PX_STATE_GROUPED_BIT;
  return 1.0f;
}

// -----------------------------------------------------------------------------
static float praxis_pointer_get_jerk_delta (px_mousekey_t* mousekey) {
  if (!IS_ACTIVATED(mousekey) || IS_PRESSING(mousekey)) {
    return 0.0f;
  }

  // Mark the mousekey as pressing; don't mark as pressed just yet.
  mousekey->state |= PX_STATE_PRESSING_BIT;

  // If the key is already released, then pass right through to released state.
  // The "jerk" in this case is the amount of time from pressed to released.
  if (IS_DEACTIVATED(mousekey)) {
    return TIMER_DIFF_16(mousekey->released, mousekey->pressed) / 1000.0f;
  }

  return timer_elapsed(mousekey->pressed) / 1000.0f;
}

// -----------------------------------------------------------------------------
static vector_t praxis_pointer_get_jerks (px_mousekeys_t* keys) {
  // Get all of the individual scalars (whether or not there is a jerk).
  const float down_scalar = praxis_pointer_get_jerk_scalar(&keys->down);
  const float left_scalar = praxis_pointer_get_jerk_scalar(&keys->left);
  const float right_scalar = praxis_pointer_get_jerk_scalar(&keys->right);
  const float up_scalar = praxis_pointer_get_jerk_scalar(&keys->up);

  // Get all of the jerk speeds for the different directions.
  const float down_speed = praxis_pointer_get_jerk_delta(&keys->down);
  const float left_speed = praxis_pointer_get_jerk_delta(&keys->left);
  const float right_speed = praxis_pointer_get_jerk_delta(&keys->right);
  const float up_speed = praxis_pointer_get_jerk_delta(&keys->up);

  // Normally we normalize the scalars, but that doesn't make much sense here.
  // We need each of the jerk speeds to be applied to each of the directions,
  // and they may be applied in different amounts; so just combine normally.
  const vector_t result = {
    .x = (right_scalar * right_speed) - (left_scalar * left_speed),
    .y = (down_scalar * down_speed) - (up_scalar * up_speed),
  };
  return result;
}
#else  // ENABLE_POINTER_JERKS

// -----------------------------------------------------------------------------
static void praxis_pointer_update_jerk_key (px_mousekey_t* mousekey) {
  if (IS_ACTIVATED(mousekey)) {
    mousekey->state |= (PX_STATE_GROUPED_BIT | PX_STATE_PRESSING_BIT);
  }
}

// -----------------------------------------------------------------------------
static vector_t praxis_pointer_get_jerks (px_mousekeys_t* keys) {
  praxis_pointer_update_jerk_key(&keys->down);
  praxis_pointer_update_jerk_key(&keys->left);
  praxis_pointer_update_jerk_key(&keys->right);
  praxis_pointer_update_jerk_key(&keys->up);
  static const vector_t result = {
    .x = 0,
    .y = 0,
  };
  return result;
}

#endif // ENABLE_POINTER_JERKS

// =============================================================================
// Velocity Processing
// =============================================================================

// -----------------------------------------------------------------------------
static float praxis_pointer_get_velocity_scalar (px_mousekey_t* mousekey) {
  if (!IS_ACTIVATED(mousekey) || !IS_PRESSING(mousekey) || IS_RELEASED(mousekey)) {
    return 0.0f;
  }

  if (IS_DEACTIVATED(mousekey)) {
    mousekey->state |= PX_STATE_RELEASING_BIT | PX_STATE_RELEASED_BIT;
    return 0.0f;
  }

  if (timer_elapsed(mousekey->pressed) > mousekey->hiccup) {
    mousekey->state |= PX_STATE_PRESSED_BIT;
  }

  return IS_PRESSED(mousekey) ? 1.0f : 0.0f;
}

// -----------------------------------------------------------------------------
static vector_t praxis_pointer_get_direction (px_mousekeys_t* keys) {
  // Get all of the individual scalars (whether or not there is movement).
  const float down_scalar = praxis_pointer_get_velocity_scalar(&keys->down);
  const float left_scalar = praxis_pointer_get_velocity_scalar(&keys->left);
  const float right_scalar = praxis_pointer_get_velocity_scalar(&keys->right);
  const float up_scalar = praxis_pointer_get_velocity_scalar(&keys->up);

  // Join the scalars together into a vector and normalize it.
  vector_t velocity = {
    .x = right_scalar - left_scalar,
    .y = down_scalar - up_scalar,
  };
  vector_normalize(&velocity);
  return velocity;
}

// -----------------------------------------------------------------------------
static bool praxis_pointer_is_moving (const px_mousekeys_t* keys) {
  return (
    IS_PRESSED(&keys->down) ||
    IS_PRESSED(&keys->left) ||
    IS_PRESSED(&keys->right) ||
    IS_PRESSED(&keys->up)
  );
}

// -----------------------------------------------------------------------------
static float praxis_pointer_get_speed (float delta, px_mousekeys_t* keys) {
  if (!praxis_pointer_is_moving(keys)) {
    keys->speed = 0.0f;
    return 0.0f;
  }

  keys->speed += delta;
  float_saturate(&keys->speed);
  return keys->speed;
}

// -----------------------------------------------------------------------------
static void praxis_handle_pointer_state (px_mousekey_t* mousekey) {
  if (IS_ACTIVATED(mousekey) && !IS_RELEASED(mousekey)) {
    return;
  }

  praxis_memzero(mousekey, sizeof(*mousekey));
}

// -----------------------------------------------------------------------------
static vector_t praxis_pointer_get_velocity (
  float delta,
  px_mousekeys_t* keys,
  float speed_start,
  float speed_end
) {
  // Update the mouse states so that if they are released they are zeroed.
  praxis_handle_pointer_state(&keys->down);
  praxis_handle_pointer_state(&keys->left);
  praxis_handle_pointer_state(&keys->right);
  praxis_handle_pointer_state(&keys->up);

  // First, let's calculate the jerks; this is the motion we get from waiting
  // to process the key (due to how the keys are pipelined, we cannot
  // immediately start processing the velocity of the cursor, so it is delayed).
  const vector_t jerks = praxis_pointer_get_jerks(keys);

  // Next let's calculate the constant raw velocity. This is the amount of
  // change that is applied during non-jerk movement (when the key is held).
  vector_t raw_velocity = praxis_pointer_get_direction(keys);
  raw_velocity.x *= delta;
  raw_velocity.y *= delta;

  // Update the current speed of the cursor (if any keys are pressed).
  float speed = speed_start + (
    (speed_end - speed_start) * praxis_pointer_get_speed(delta, keys)
  );

  const vector_t result = {
    .x = speed * (jerks.x + raw_velocity.x),
    .y = speed * (jerks.y + raw_velocity.y),
  };
  return result;
}

// -----------------------------------------------------------------------------
static int8_t praxis_handle_pointer_movement_axis (
  float* offset,
  float velocity
) {
  if (velocity == 0.0f) {
    return 0;
  }

  *offset += velocity;

  // Break the resultant offset apart to see what should be returned.
  float output;
  *offset = modff(*offset, &output);

  // Cast back to an integer, we can only send the values [-127, 127].
  return int32_clamp8(output, -127, 127);
}

// =============================================================================
// Cursor Velocity Processing
// =============================================================================

// -----------------------------------------------------------------------------
static vector_t praxis_pointer_get_cursor_velocity (float delta) {
  float speed = 1.0f;
  if (s_mode & PX_MODE_LEFT_ZETTA_BIT) {
    speed /= 3.0f;
  }
  if (s_mode & PX_MODE_RIGHT_ZETTA_BIT) {
    speed /= 3.0f;
  }
  return praxis_pointer_get_velocity(
    delta,
    &s_mousestate.move,
    speed * MOUSE_SPEED_START,
    speed * MOUSE_SPEED_END
  );
}

// -----------------------------------------------------------------------------
static void paxis_pointer_update_cursor (
  report_mouse_t* mouse_report,
  float delta
) {
  const vector_t velocity = praxis_pointer_get_cursor_velocity(delta);
  mouse_report->x = praxis_handle_pointer_movement_axis(&s_mousestate.x, velocity.x);
  mouse_report->y = praxis_handle_pointer_movement_axis(&s_mousestate.y, velocity.y);
}

// =============================================================================
// Wheel Velocity Processing
// =============================================================================

// -----------------------------------------------------------------------------
static vector_t praxis_pointer_get_wheel_velocity (float delta) {
  float speed = 1.0f;
  if (s_mode & PX_MODE_LEFT_ZETTA_BIT) {
    speed /= 3.0f;
  }
  if (s_mode & PX_MODE_RIGHT_ZETTA_BIT) {
    speed /= 3.0f;
  }
  return praxis_pointer_get_velocity(
    delta,
    &s_mousestate.wheel,
    speed * MOUSE_WHEEL_SPEED_START,
    speed * MOUSE_WHEEL_SPEED_END
  );
}

// -----------------------------------------------------------------------------
static int8_t praxis_pointer_get_wheel_hiccup_from_state (px_mousekey_t* key) {
  if (!IS_ACTIVATED(key) || IS_USERBIT(key)) {
    return 0;
  }

  key->state |= PX_STATE_USER_BIT;
  return 1;
}

// -----------------------------------------------------------------------------
static int8_t praxis_pointer_get_wheel_hiccup (px_mousekey_t* lesser, px_mousekey_t* greater) {
  return praxis_pointer_get_wheel_hiccup_from_state(greater) -
    praxis_pointer_get_wheel_hiccup_from_state(lesser);
}

// -----------------------------------------------------------------------------
static void paxis_pointer_update_wheel (report_mouse_t* mouse_report, float delta) {
  const vector_t wheel_velocity = praxis_pointer_get_wheel_velocity(delta);
  mouse_report->h =
    praxis_handle_pointer_movement_axis(&s_mousestate.h, wheel_velocity.x) +
    praxis_pointer_get_wheel_hiccup(&s_mousestate.wheel.left, &s_mousestate.wheel.right);
  mouse_report->v =
    -praxis_handle_pointer_movement_axis(&s_mousestate.v, wheel_velocity.y) -
    praxis_pointer_get_wheel_hiccup(&s_mousestate.wheel.up, &s_mousestate.wheel.down);
}

// =============================================================================
// Main Handler
// =============================================================================

// -----------------------------------------------------------------------------
void praxis_handle_pointer (report_mouse_t* mouse_report) {
  // Calculate the current delta for this pointer update.
  static qmk_timestamp_t last_timer = 0;
  const float delta = timer_elapsed(last_timer) / 1000.0f;
  last_timer = timer_read();

  // Update all of the individual elements of the mouse report.
  mouse_report->buttons = s_mousestate.buttons;
  paxis_pointer_update_cursor(mouse_report, delta);
  paxis_pointer_update_wheel(mouse_report, delta);
}
