/*******************************************************************************
 * PRAXIS EVENT HANDLERS (KEY EVENT)
 ******************************************************************************/

// -----------------------------------------------------------------------------
static void praxis_handle_event_pressed (
  px_hw_keycode_t    keycode,
  const keyrecord_t* record
) {
  // Even if the key gets swallowed or re-pressed, we want to count it.
  uint32_safe_inc(&s_stats.counters.keys);

  // See if the key is currently pressed and awaiting release. The only time
  // a key will be in such a state in the keystate array is if it were already
  // pressed. In this case, don't re-press the key, just don't unpress it.
  px_keystate_t* keystate = get_keystate(keycode);
  if (keystate != NULL) {
    if (IS_RELEASED(keystate)) {
      keystate->state &= ~PX_STATE_PRESSED_BIT;
    }
    keystate->state &= PX_STATE_DEACTIVATED_BIT - 1;
    return;
  }

  // Add the key to the list of keystates - the key will be handled in update.
  praxis_keystate_add(keycode, record);

  // Ensure that the key press gets recorded in the event log for debugging.
  praxis_add_event_log(keycode, PX_EVENT_PRESS);
}

// -----------------------------------------------------------------------------
static void praxis_handle_event_released (
  px_hw_keycode_t    keycode,
  const keyrecord_t* record
) {
  px_keystate_t* keystate = get_keystate(keycode);
  if (keystate == NULL) {
    s_last_error = PX_SWALLOWED_KEY_RELEASED;
    return; // Key was swallowed.
  }

  // We just need to set the releasing bit; the event loop handles the rest.
  keystate->state |= PX_STATE_DEACTIVATED_BIT;
  keystate->released = record->event.time;

  // Ensure that the key release gets recorded in the event log for debugging.
  praxis_add_event_log(keycode, PX_EVENT_RELEASE);
}

// =============================================================================
// Main Handler
// =============================================================================

// -----------------------------------------------------------------------------
static void praxis_handle_event (
  px_hw_keycode_t    keycode,
  const keyrecord_t* record
) {
  if (record->event.pressed) {
    praxis_handle_event_pressed(keycode, record);
  } else {
    praxis_handle_event_released(keycode, record);
  }
}
