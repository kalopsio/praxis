/*******************************************************************************
 * PRAXIS GLOBALS
 ******************************************************************************/

// Various mode bits are set here, see px_mode_bit_t for details.
static px_mode_t s_mode = 0;

// Each group consumes a bit from this value - so 8 groups max.
static uint32_t s_groups = 0;

// The mouse state so that we can send it back to the pointer driver.
static px_mousestate_t s_mousestate = {};

// Information about the active keystates.
static px_keystate_t s_raw_keystates[MAX_KEYS_HELD + 2] = {};
static px_keystate_t* const s_keystates = &s_raw_keystates[1];
static uint8_t s_keystates_count = 0;
#define KEYSTATES_BEGIN (s_keystates)
#define KEYSTATES_END (&s_keystates[s_keystates_count])
#define KEYSTATES_RBEGIN (&s_keystates[s_keystates_count - 1])
#define KEYSTATES_REND (&s_keystates[-1])

// Statistics about how many keys you pressed and your WPM.
static px_stats_t s_stats = {};

// The state of the OLED for the primary display.
static px_oled_state_t s_oled_state = PX_OLED_STATE_INACTIVE;

// The event objects for the debug event lot display.
#define MAX_EVENT_LOG_SIZE 16
static px_key_event_t s_event_log[MAX_EVENT_LOG_SIZE];

// The last error to occur on the primary microcontroller.
static px_error_t s_last_error = PX_ERROR_NONE;
