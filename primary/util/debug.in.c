/*******************************************************************************
 * PRAXIS DEBUG
 ******************************************************************************/

// -----------------------------------------------------------------------------
#if       defined(ENABLE_DEBUG) && PRAXIS_TARGET == PRAXIS_PRIMARY
# include "debug/enabled.in.c"
#else  // defined(ENABLE_DEBUG) && PRAXIS_TARGET == PRAXIS_PRIMARY
# include "debug/disabled.in.c"
#endif // defined(ENABLE_DEBUG) && PRAXIS_TARGET == PRAXIS_PRIMARY
