/*******************************************************************************
 * PRAXIS TYPES
 ******************************************************************************/
#pragma once

// =============================================================================
// General Types
// =============================================================================

// The QMK Keycode (separate logically from the Praxis keycodes). This is the
// keycode that will actually be acted upon logically for sending a key event to
// the Operating System.
typedef uint16_t qmk_keycode_t;

// =============================================================================
// Modes
// =============================================================================

// Different Praxis layers as indicated by the mode of the keyboard.
// Use these to assign to the keymap below to ensure the correct assignments.
#define PL_EDIT_LAYER1 (0)
#define PL_EDIT_SHIFT1 (PX_MODE_RUNE_SHIFT_BIT)
#define PL_EDIT_LAYER2 (PX_MODE_LAYER_SHIFT_BIT)
#define PL_EDIT_SHIFT2 (PX_MODE_LAYER_SHIFT_BIT | PX_MODE_RUNE_SHIFT_BIT)
#define PL_REVIEW_LAYER1 (PX_MODE_REVIEW_MODE_BIT)
#define PL_REVIEW_SHIFT1 (PX_MODE_REVIEW_MODE_BIT | PX_MODE_RUNE_SHIFT_BIT)
#define PL_REVIEW_LAYER2 (PX_MODE_REVIEW_MODE_BIT | PX_MODE_LAYER_SHIFT_BIT)
#define PL_REVIEW_SHIFT2 (PX_MODE_REVIEW_MODE_BIT | PX_MODE_LAYER_SHIFT_BIT | PX_MODE_RUNE_SHIFT_BIT)
#define PX_MODE_LAYER_MASK (PX_MODE_RUNE_SHIFT_BIT | PX_MODE_LAYER_SHIFT_BIT | PX_MODE_REVIEW_MODE_BIT)

// The mode of the keyboard as extracted from the s_mode value.
typedef enum px_keyboard_mode_t {
  PX_KEYBOARD_MODE_EDIT,
  PX_KEYBOARD_MODE_REVIEW,
  PX_KEYBOARD_MODE_KILL,
} px_keyboard_mode_t;

// =============================================================================
// Key States
// =============================================================================

// The state of a given key, this is used to determine what to do next with it.
// It is comprised of a set of bits for each state; see: px_state_bit_t
typedef uint8_t px_state_t;

// Various states that the keys can be in. States in Praxis are based on bits
// that get set, and in order for a key to be fully handled all bits must be set
// (e.g. all states must have been visited.)
enum px_state_bit_t {
  // The key is activated, this bit is set just to differentiate the state from
  // inactive (all 0s). It doesn't actually do anything else interesting.
  PX_STATE_ACTIVATED_BIT = (1 << 0),

  // The key has been processed and is now in a group of keys that all count as
  // one press. This group could be a group of size 1, but nonetheless the key
  // is no longer suitable for joining with other keys.
  PX_STATE_GROUPED_BIT = (1 << 1),

  // The key is approved to move into the pressed state. If this bit is not set,
  // then other keys may be blocking it from progressing into a pressed state,
  // or the key may be deciding whether it's a press or a hold key.
  PX_STATE_PRESSING_BIT = (1 << 2),

  // The key has been processed as pressed, this is the state where the key is
  // finally sent as a key down event to the computer for processing by the OS.
  PX_STATE_PRESSED_BIT = (1 << 3),

  // The key is deactivated, this bit is set when the key is physically released
  // from being held, and initiates the process of releasing the key logically.
  PX_STATE_DEACTIVATED_BIT = (1 << 4),

  // The key is appoved to move into the released state. If this bit is not set,
  // then the key may be artificially held for short period of time.
  PX_STATE_RELEASING_BIT = (1 << 5),

  // The key has been processed as a release, this is the state where the key is
  // finally sent as a key up event to the computer for processing by the OS.
  PX_STATE_RELEASED_BIT = (1 << 6),

  // A final bit that can be used for implementation-specific purposes.
  PX_STATE_USER_BIT = (1 << 7),
  PX_STATE_FINISHED_BIT = PX_STATE_USER_BIT,
};

// Macro helpers for testing the state of the key.
#define IS_ACTIVATED_STATE(state)    ((state) & PX_STATE_ACTIVATED_BIT)
#define IS_GROUPED_STATE(state)      ((state) & PX_STATE_GROUPED_BIT)
#define IS_PRESSING_STATE(state)     ((state) & PX_STATE_PRESSING_BIT)
#define IS_PRESSED_STATE(state)      ((state) & PX_STATE_PRESSED_BIT)
#define IS_DEACTIVATED_STATE(state)  ((state) & PX_STATE_DEACTIVATED_BIT)
#define IS_RELEASING_STATE(state)    ((state) & PX_STATE_RELEASING_BIT)
#define IS_RELEASED_STATE(state)     ((state) & PX_STATE_RELEASED_BIT)
#define IS_USERBIT_STATE(state)      ((state) & PX_STATE_USER_BIT)
#define IS_FINISHED_STATE(state)     ((state) & PX_STATE_FINISHED_BIT)

// Macro helpers for testing the state of the key.
#define IS_ACTIVATED(keystate)       IS_ACTIVATED_STATE((keystate)->state)
#define IS_GROUPED(keystate)         IS_GROUPED_STATE((keystate)->state)
#define IS_PRESSING(keystate)        IS_PRESSING_STATE((keystate)->state)
#define IS_PRESSED(keystate)         IS_PRESSED_STATE((keystate)->state)
#define IS_DEACTIVATED(keystate)     IS_DEACTIVATED_STATE((keystate)->state)
#define IS_RELEASING(keystate)       IS_RELEASING_STATE((keystate)->state)
#define IS_RELEASED(keystate)        IS_RELEASED_STATE((keystate)->state)
#define IS_USERBIT(keystate)         IS_USERBIT_STATE((keystate)->state)
#define IS_FINISHED(keystate)        IS_FINISHED_STATE((keystate)->state)

// This structure is important for keeping track of the various key states.
// It's used to implement some form of chording so that we can react to when
// multiple keys are pressed at the same time.
typedef struct px_keystate_t {
  // This is assigned immediately, and will be set for the entire key life.
  px_keycode_t px_keycode;

  // This is only assigned after the key is sampled (PRESSED state).
  // Until then, this is assigned to 0 which means no key press.
  qmk_keycode_t qmk_keycode;

  // This is the state of the key, see `px_state_t` for more info.
  px_state_t state;

  // This is the group of the key, this is finalized after the key is PRESSED.
  uint8_t group;

  // The time that the key is pressed, this is set immediately on ACTIVE.
  qmk_timestamp_t pressed;

  // The time that the key is releasing, this is set immediately on RELEASING.
  qmk_timestamp_t released;

  // A counter variable that can be used for various purposes.
  uint8_t count;
} px_keystate_t;

// A structure that holds a set of keys that are all being pressed at once.
typedef struct px_keycombo_t {
  px_keystate_t* begin;
  px_keystate_t* end;
} px_keycombo_t;

// =============================================================================
// Mouse State
// =============================================================================

// Information about the state of a single mousekey.
typedef struct px_mousekey_t {
  // The current state of the mousekey.
  px_state_t state;

  // When the mousekey was pressed.
  qmk_timestamp_t pressed;

  // When the mousekey was released (used for processing jerks).
  qmk_timestamp_t released;

  // Used for processing a "hiccup", which is where the mouse key does a small
  // amount of movement, and then it will continue with normal movement after.
  qmk_timestamp_t hiccup;
} px_mousekey_t;

// Information about all of the mousekeys, and shared details.
typedef struct px_mousekeys_t {
  // The speed that should be applied to all the mousekeys.
  float speed;

  // The left directional key.
  px_mousekey_t left;

  // The right directional key.
  px_mousekey_t right;

  // The down directional key.
  px_mousekey_t down;

  // The up directional key.
  px_mousekey_t up;
} px_mousekeys_t;

// Information about the entire mouse state (wheel and movement).
typedef struct px_mousestate_t {
  // The buttons that are pressed (see: KC_MS_BTN1).
  uint8_t buttons;

  // The mousekeys for cursor movement.
  px_mousekeys_t move;

  // The mousekeys for wheel movement.
  px_mousekeys_t wheel;

  // The fractional x component not yet sent to the driver.
  float x;

  // The fractional y component not yet sent to the driver.
  float y;

  // The fractional h component (wheel x) not yet sent to the driver.
  float h;

  // The fractional v component (wheel y) not yet sent to the driver.
  float v;
} px_mousestate_t;

// Information about the WPM calculated by the keyboard.
#define PX_WPM_SAMPLES_MAX 61
typedef struct px_wpm_stats_t {
  // The max WPM reached since the keyboard was plugged in.
  uint32_t max;

  // The current samples for the current WPM calculations (each is 1s).
  uint8_t samples[PX_WPM_SAMPLES_MAX];

  // A timer for controlling rollover to the new second.
  qmk_timestamp_t timer;

  // An index number to let the secondary controller know when a second passed.
  uint8_t index;
} px_wpm_stats_t;

// =============================================================================
// Metrics State
// =============================================================================

// The format of a word as we are processing it.
typedef enum px_word_format_t {
  PX_WORD_FORMAT_UNKNOWN,
  PX_WORD_FORMAT_UNCERTAIN,
  PX_WORD_FORMAT_LOWERCASE,
  PX_WORD_FORMAT_UPPERCASE,
  PX_WORD_FORMAT_TITLECASE,
} px_word_format_t;

// The classification of a character as we are processing it.
typedef enum px_char_class_t {
  PX_CHAR_UNDEFINED,
  PX_CHAR_ALPHA_LOWER,
  PX_CHAR_ALPHA_UPPER,
  PX_CHAR_SYMBOL,
} px_char_class_t;

// The current word that we are processing in the WPM calculator.
typedef struct px_word_stats_t {
  // The current understood format of the word.
  px_word_format_t format;

  // The last character's class for determining format.
  px_char_class_t last_class;

  // The length or position in the word.
  uint16_t length;

  // The index into the WPM samples for when the word was recorded.
  uint8_t index;
} px_word_stats_t;

// Generic counters for certain events.
typedef struct px_counter_stats_t {
  // Printable characters emitted.
  uint32_t chars;

  // Words typed into the keyboard.
  uint32_t words;

  // Keys typed into the keyboard.
  uint32_t keys;
} px_counter_stats_t;

// Statistic that the keyboard keeps track of.
typedef struct px_stats_t {
  // Counters for various events.
  px_counter_stats_t counters;

  // WPM calculations ongoing.
  px_wpm_stats_t wpm;

  // Word calculations ongoing.
  px_word_stats_t word;
} px_stats_t;

// =============================================================================
// Event Logs
// =============================================================================

// An enum for specifying what kind of key event happened in the event log.
typedef enum px_key_event_type_t {
  // The key was pressed.
  PX_EVENT_PRESS = 0,

  // The key was released.
  PX_EVENT_RELEASE = (1 << 6),

  // The key was tapped (pressed then released before any other key press).
  PX_EVENT_TAP = (1 << 7),
} px_key_event_type_t;

// An event entry in the event log. This is of the format of a keydiff | key
// event type. This is done primarily to save space on the firmware.
typedef uint8_t px_key_event_t;

#define PX_KEYDIFF_MASK 0x3F

// =============================================================================
// Error Type
// =============================================================================

typedef enum px_error_t {
  // A default value for the error enum, indicates no error.
  PX_ERROR_NONE,

  // Too many keys were pressed and we had to swallow a key press. This usually
  // turns into a PX_SWALLOWED_KEY_RELEASED after releasing the key.
  PX_SWALLOWED_KEY,

  // An error when a key was scheduled for release, but we were unable to find
  // the pressed key in the list of keystates.
  PX_SWALLOWED_KEY_RELEASED,

  // More groups than should be present have been allocated by the keyboard.
  // We usually don't expect more than 8 groups at a time. If we experience more
  // than this, there is likely something wrong.
  PX_GROUP_ALLOCATION_LEAK,
} px_error_t;
