/*******************************************************************************
 * PRAXIS DEBUG (Enabled)
 ******************************************************************************/

// -----------------------------------------------------------------------------
static bool s_debug_line = false;

// -----------------------------------------------------------------------------
#undef STR
#undef U8

// -----------------------------------------------------------------------------
// Called at the end of every update, for breaking up the debug text to make it
// easier to look at.
// -----------------------------------------------------------------------------
static void debug_finalize (void) {
  if (s_debug_line) {
    s_debug_line = false;
    SEND_STRING("\n");
  }
}

// -----------------------------------------------------------------------------
// Allows for sensible variadic macro arguments. Depending on the number of
// arguments this will expand to one of the following.
// -----------------------------------------------------------------------------
#define DEBUG(...) GET_MACRO(__VA_ARGS__, _DEBUG_3, _DEBUG_2, _DEBUG_1)(__VA_ARGS__)
#define _DEBUG_3(a, b, c) \
  _DEBUG_START(); _DEBUG_##a; _DEBUG_##b; _DEBUG_##c; _DEBUG_END()
#define _DEBUG_2(a, b) \
  _DEBUG_START(); _DEBUG_##a; _DEBUG_##b; _DEBUG_END()
#define _DEBUG_1(a) \
  _DEBUG_START(); _DEBUG_##a; _DEBUG_END()

// -----------------------------------------------------------------------------
// Helper macros for formatting debug macros.
// -----------------------------------------------------------------------------
#define _DEBUG_START() do { s_debug_line = true
#define _DEBUG_END() SEND_STRING(":"); } while (0)
#define _DEBUG_STR(arg) SEND_STRING(arg)
#define _DEBUG_U8(arg) send_byte(arg)

// -----------------------------------------------------------------------------
// Prints a constant without going through any of the debug logic.
// -----------------------------------------------------------------------------
#define print_constant(c) SEND_STRING(c)

// -----------------------------------------------------------------------------
void print_digit (uint8_t digit) {
  switch (digit) {
    case 0: print_constant("0"); break;
    case 1: print_constant("1"); break;
    case 2: print_constant("2"); break;
    case 3: print_constant("3"); break;
    case 4: print_constant("4"); break;
    case 5: print_constant("5"); break;
    case 6: print_constant("6"); break;
    case 7: print_constant("7"); break;
    case 8: print_constant("8"); break;
    case 9: print_constant("9"); break;
    default: break;
  }
}

// -----------------------------------------------------------------------------
void print_integer (uint32_t value) {
  const uint32_t div = value / 10;
  const uint32_t rem = value % 10;
  if (div > 0) {
    print_integer(div);
  }
  print_digit(rem);
}

// -----------------------------------------------------------------------------
void print_float (float value) {
  float y;
  const float x = modff(value, &y);

  // Cast down to integers to do some integer math.
  const uint32_t xi = x * 100000;
  const uint32_t yi = y;

  // Print the numbers
  print_integer(yi);
  print_constant(".");
  if (xi < 100000) print_constant("0");
  if (xi < 10000) print_constant("0");
  if (xi < 1000) print_constant("0");
  if (xi < 100) print_constant("0");
  if (xi < 10) print_constant("0");
  print_integer(xi);
}
